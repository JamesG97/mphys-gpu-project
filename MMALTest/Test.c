#include "bcm_host.h"


#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES/gl.h>
#include <GLES/glext.h>
#include <GLES2/gl2.h>

#include "interface/mmal/mmal.h"
#include "interface/mmal/mmal_logging.h"
#include "interface/mmal/mmal_buffer.h"
#include "interface/mmal/util/mmal_util.h"
#include "interface/mmal/util/mmal_util_params.h"
#include "interface/mmal/util/mmal_default_components.h"
#include "interface/mmal/util/mmal_connection.h"
#include "interface/mmal/mmal_parameters_camera.h"
#include "interface/vcos/vcos.h"

#include <semaphore.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

/*
 * Below lies my horrible attempt at using MMAL in C
 * I'm sorry if you're reading this
 * 
 * Still needed: 
 * 	Ports must be accessed on the camera component
 * 	Data must be sent to shared memory
 * 	Python must somehow access this data and run a fragment shader on it
 * 	Python must finally output the manipulated image to the screen
 * 
 */

/// Definitions
// Standard port setting for the camera component
#define MMAL_CAMERA_PREVIEW_PORT 0
#define MMAL_CAMERA_VIDEO_PORT 1
#define MMAL_CAMERA_CAPTURE_PORT 2

#define MMAL_PREVIEW_BUFFER_NUM 2

/// Structs
// State struct, keeps track of all components, ports, and connections
typedef struct RUN_STATE
{
	int width;
	int height;															// Screen resolution
	
	MMAL_COMPONENT_T *camera_component;									// Camera component
} RUN_STATE;

// Raspitex state for storing the state of the texture generation process
// Modified from	https://github.com/raspberrypi/userland/blob/master/host_applications/linux/apps/raspicam/RaspiTex.h   line 102
typedef struct RASPITEX_STATE
{	
	int x;
	int y;																// Offset
	int width;
	int height;															// Texture and buffer resolutions (ideally resolution of camera)
	   
	DISPMANX_DISPLAY_HANDLE_T disp;     								// Dispmanx display for GL preview
	EGL_DISPMANX_WINDOW_T win;          								// Dispmanx handle for preview surface
	
	int opacity;                        								// Alpha value for display element
	
	EGLNativeWindowType* native_window; 								// Native window used for EGL surface
	EGLDisplay display;                 								// The current EGL display
	EGLSurface surface;                 								// The current EGL surface
	EGLContext context;                 								// The current EGL context
	const EGLint *egl_config_attribs;   								// GL scenes preferred EGL configuration

	
	MMAL_PORT_T *preview_port;											// Preview port (where to get frame buffer from)	
	MMAL_POOL_T *preview_pool; 											// Preview pool for storing all buffers
	MMAL_QUEUE_T *preview_queue;										// Queue of buffers to be viewed
	VCOS_THREAD_T preview_thread;										// Multithreading for preview functionality
		
	uint texture_id;                     								// Texture ID
} RASPITEX_STATE;



/// Forward declarations
// Function to print the status of an operation
static void print_status(char *contextMessage, MMAL_STATUS_T status);

// Function to destroy instances of components
static MMAL_STATUS_T cleanup();



/// Variables & instances of structs
RUN_STATE state;														// Keep track of current state of the program
RASPITEX_STATE tex_state;												// Keep track of the state of the texture capture manager
MMAL_STATUS_T status;													// Keep track of status of the program (MMAL_SUCCESS = good)
MMAL_PORT_T *camera_still_port;											
MMAL_PORT_T *camera_preview_port;										// Keep track of camera's still image & preview ports

/// The main method
int main(int argc, char **argv)
{
	/// Setup dependencies
	// Initialise Broadcom package
	bcm_host_init();
	printf("BCM init ran okay\n");
	
	// Initialise vcos
	vcos_init();
	printf("vcos init ran okay\n");
	
	
	/// Set up state
	// Get screen size
	graphics_get_display_size(0, &state.width, &state.height);
	
	// Create a camera component + print the status of the camera creation, and the camera's name
    status = mmal_component_create(MMAL_COMPONENT_DEFAULT_CAMERA, &state.camera_component);
    print_status("Camera component creation: ", status);
	printf("Camera name: %s\n", state.camera_component->name);
	
	// Set up the camera ports
	camera_still_port   = state.camera_component->output[MMAL_CAMERA_CAPTURE_PORT];
	camera_preview_port = state.camera_component->output[MMAL_CAMERA_PREVIEW_PORT];
	
	
	/// Set up tex_state
	// Set up the tex_state struct	
	glGenTextures(1, &tex_state.texture_id);
	tex_state.preview_port = camera_preview_port;
	tex_state.width = 1024;
	tex_state.height = 1024;
	
	
	/// Finish 
	// Clean everything up, delete created components
	print_status("Clean up: ", cleanup());
	
	return 0;
}



/// Full declarations of functions
// A simple function to print to the terminal the status of the 
static void print_status(char *context_message, MMAL_STATUS_T status)
{
	// Print a bit of text describing what the status is for
    printf(context_message);
    
    // Print the status
	switch(status)
	{
		/* 
		 * TO DO:
		 * 			Add all other status possibilities
		 *
		 */
		
		case MMAL_SUCCESS:
		{
			printf("Success\n");
			break;
		}
		default:
		{
			printf("Not success\n");
			break;
		}
	}
}

// Stolen from   	https://github.com/raspberrypi/userland/blob/master/host_applications/linux/apps/raspicam/RaspiTexUtil.c   line 329
/**
 * Advances the texture and EGL image to the next MMAL buffer.
 *
 * @param display The EGL display.
 * @param target The EGL image target e.g. EGL_IMAGE_BRCM_MULTIMEDIA
 * @param mm_buf The EGL client buffer (mmal opaque buffer) that is used to
 * create the EGL Image for the preview texture.
 * @param egl_image Pointer to the EGL image to update with mm_buf.
 * @param texture Pointer to the texture to update from EGL image.
 * @return Zero if successful.
 */
int raspitexutil_do_update_texture(EGLDisplay display, EGLenum target, EGLClientBuffer mm_buf, GLuint *texture, EGLImageKHR *egl_image)
{
   vcos_log_trace("%s: mm_buf %u", VCOS_FUNCTION, (unsigned) mm_buf);
   glBindTexture(GL_TEXTURE_EXTERNAL_OES, *texture);
   if (*egl_image != EGL_NO_IMAGE_KHR)
   {
      /* Discard the EGL image for the preview frame */
      eglDestroyImageKHR(display, *egl_image);
      *egl_image = EGL_NO_IMAGE_KHR;
   }

   *egl_image = eglCreateImageKHR(display, EGL_NO_CONTEXT, target, mm_buf, NULL);
   glEGLImageTargetTexture2DOES(GL_TEXTURE_EXTERNAL_OES, *egl_image);

   return 0;
}

// Destroy all components
static MMAL_STATUS_T cleanup()
{
	MMAL_STATUS_T overall_status = MMAL_SUCCESS;
	MMAL_STATUS_T cleanup_status = MMAL_SUCCESS;
	
	// If the camera component has been set, destroy it
	if (state.camera_component != NULL)
	{
		// Get status of destroy attempt
		cleanup_status = mmal_component_destroy(state.camera_component);
		
		// If it was not a success, set the overall cleanup status to it and print what went wrong
		if(cleanup_status != MMAL_SUCCESS)
		{
			print_status("Camera destroy: ", cleanup_status);
			overall_status =  cleanup_status;
		}
	}
	
	// Finally, return the overall status for the cleanup operation
	return overall_status;
}



int raspitexutil_create_native_window(RASPITEX_STATE *raspitex_state)
{
	VC_DISPMANX_ALPHA_T alpha = {DISPMANX_FLAGS_ALPHA_FIXED_ALL_PIXELS, 255, 0};
	VC_RECT_T src_rect = {0};
	VC_RECT_T dest_rect = {0};
	uint32_t disp_num = 0; // Primary
	uint32_t layer_num = 0;
	DISPMANX_ELEMENT_HANDLE_T elem;
	DISPMANX_UPDATE_HANDLE_T update;

	alpha.opacity = raspitex_state->opacity;
	dest_rect.x = raspitex_state->x;
	dest_rect.y = raspitex_state->y;
	dest_rect.width = raspitex_state->width;
	dest_rect.height = raspitex_state->height;

	vcos_log_trace("%s: %d,%d,%d,%d %d,%d,0x%x,0x%x", VCOS_FUNCTION,
				  src_rect.x, src_rect.y, src_rect.width, src_rect.height,
				  dest_rect.x, dest_rect.y, dest_rect.width, dest_rect.height);

	src_rect.width = dest_rect.width << 16;
	src_rect.height = dest_rect.height << 16;

	raspitex_state->disp = vc_dispmanx_display_open(disp_num);
	if (raspitex_state->disp == DISPMANX_NO_HANDLE)
	{
		vcos_log_error("Failed to open display handle");
		goto error;
	}

	update = vc_dispmanx_update_start(0);
	if (update == DISPMANX_NO_HANDLE)
	{
		vcos_log_error("Failed to open update handle");
		goto error;
	}

	elem = vc_dispmanx_element_add(update, raspitex_state->disp, layer_num,
								   &dest_rect, 0, &src_rect, DISPMANX_PROTECTION_NONE, &alpha, NULL,
                                   DISPMANX_NO_ROTATE);
	if (elem == DISPMANX_NO_HANDLE)
	{
		vcos_log_error("Failed to create element handle");
		goto error;
	}

	raspitex_state->win.element = elem;
	raspitex_state->win.width = raspitex_state->width;
	raspitex_state->win.height = raspitex_state->height;
	vc_dispmanx_update_submit_sync(update);

	raspitex_state->native_window = (EGLNativeWindowType*) &raspitex_state->win;

	return 0;
error:
	return -1;
}


