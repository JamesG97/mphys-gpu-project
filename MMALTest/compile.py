import os


# PROVIDE DATA NEEDED
sourceFile      = "Test"
linkDirectories = ["/opt/vc/include", "/opt/vc/lib"]
libs            = ["EGL", "GL", "GLU", "glut", "bcm_host", "pthread", "vcos", "mmal", "mmal_components", "mmal_core"]


# CONSTRUCT THE COMMAND
# Compile fileName.c using gcc
instructionString = "gcc " + sourceFile + ".c"

# With links to given directories
for ld in linkDirectories:
    instructionString += " -I" + ld
    instructionString += " -L" + ld

# Saving as fileName.a
instructionString += " -o " + sourceFile + ".a"

# Using the libraries given
for l in libs:
    instructionString += " -l" + l


# SHOW AND RUN THE COMMAND
print("Compiling with python, with instruction string \n\t" + instructionString)
os.system(instructionString)
