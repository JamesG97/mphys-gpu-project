#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
int main(void) 
{
	Display *display;
	Window window;
	XEvent event;
	int screenID;

	// Create X11 display object
	display = XOpenDisplay(NULL);
	if (display == NULL)
	{
		fprintf(stderr, "Cannot open display\n");
		exit(1);
	}

	// Setup window
	screenID = DefaultScreen(display);
	window = XCreateSimpleWindow(display, RootWindow(display, screenID), 
								 10, 10, 640, 480, 1, BlackPixel(display, screenID), 
								 WhitePixel(display, screenID));
	XSelectInput(display, window, ExposureMask | KeyPressMask);
	// Display window
	XMapWindow(display, window);

	const char *msg = "Hello, World!";
	while (1) 
	{
		XNextEvent(display, &event);
		if (event.type == Expose) 
		{
			XFillRectangle(display, window, DefaultGC(display, screenID), 20, 20, 10, 10);
			XDrawString(display, window, DefaultGC(display, screenID), 40, 30, msg, strlen(msg));
		}
		if (event.type == KeyPress) break;
	}

	XCloseDisplay(display);
	return 0;
}
