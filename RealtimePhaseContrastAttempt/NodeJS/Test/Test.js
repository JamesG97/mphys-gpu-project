var http = require('http');
var dt = require('./ModuleTest');

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  
  res.write(dt.DateTime());
  
  res.end();
}).listen(8080);