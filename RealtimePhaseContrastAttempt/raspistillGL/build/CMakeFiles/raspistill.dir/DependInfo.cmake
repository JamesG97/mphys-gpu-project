# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/RaspiCLI.c" "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/build/CMakeFiles/raspistill.dir/RaspiCLI.o"
  "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/RaspiCamControl.c" "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/build/CMakeFiles/raspistill.dir/RaspiCamControl.o"
  "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/RaspiCommonSettings.c" "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/build/CMakeFiles/raspistill.dir/RaspiCommonSettings.o"
  "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/RaspiHelpers.c" "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/build/CMakeFiles/raspistill.dir/RaspiHelpers.o"
  "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/RaspiPreview.c" "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/build/CMakeFiles/raspistill.dir/RaspiPreview.o"
  "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/RaspiStill.c" "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/build/CMakeFiles/raspistill.dir/RaspiStill.o"
  "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/RaspiTex.c" "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/build/CMakeFiles/raspistill.dir/RaspiTex.o"
  "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/RaspiTexUtil.c" "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/build/CMakeFiles/raspistill.dir/RaspiTexUtil.o"
  "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/gl_scenes/square.c" "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/build/CMakeFiles/raspistill.dir/gl_scenes/square.o"
  "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/tga.c" "/home/pi/mphys-gpu-project/RealtimePhaseContrastAttempt/raspistillGL/build/CMakeFiles/raspistill.dir/tga.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GIT_TAINTED=37"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/opt/vc/userland/host_applications/linux/libs/bcm_host/include"
  "/opt/vc/userland/host_applications/linux/libs/sm"
  "/opt/vc/userland"
  "../"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
