# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/pi/mphys-gpu-project/raspistill/RaspiCLI.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/RaspiCLI.o"
  "/home/pi/mphys-gpu-project/raspistill/RaspiCamControl.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/RaspiCamControl.o"
  "/home/pi/mphys-gpu-project/raspistill/RaspiCommonSettings.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/RaspiCommonSettings.o"
  "/home/pi/mphys-gpu-project/raspistill/RaspiGPS.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/RaspiGPS.o"
  "/home/pi/mphys-gpu-project/raspistill/RaspiHelpers.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/RaspiHelpers.o"
  "/home/pi/mphys-gpu-project/raspistill/RaspiPreview.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/RaspiPreview.o"
  "/home/pi/mphys-gpu-project/raspistill/RaspiStill.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/RaspiStill.o"
  "/home/pi/mphys-gpu-project/raspistill/RaspiTex.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/RaspiTex.o"
  "/home/pi/mphys-gpu-project/raspistill/RaspiTexUtil.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/RaspiTexUtil.o"
  "/home/pi/mphys-gpu-project/raspistill/gl_scenes/mirror.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/gl_scenes/mirror.o"
  "/home/pi/mphys-gpu-project/raspistill/gl_scenes/models.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/gl_scenes/models.o"
  "/home/pi/mphys-gpu-project/raspistill/gl_scenes/sobel.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/gl_scenes/sobel.o"
  "/home/pi/mphys-gpu-project/raspistill/gl_scenes/square.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/gl_scenes/square.o"
  "/home/pi/mphys-gpu-project/raspistill/gl_scenes/teapot.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/gl_scenes/teapot.o"
  "/home/pi/mphys-gpu-project/raspistill/gl_scenes/vcsm_square.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/gl_scenes/vcsm_square.o"
  "/home/pi/mphys-gpu-project/raspistill/gl_scenes/yuv.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/gl_scenes/yuv.o"
  "/home/pi/mphys-gpu-project/raspistill/libgps_loader.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/libgps_loader.o"
  "/home/pi/mphys-gpu-project/raspistill/tga.c" "/home/pi/mphys-gpu-project/raspistill/build/CMakeFiles/raspistill.dir/tga.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GIT_TAINTED=12"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/opt/vc/userland/host_applications/linux/libs/bcm_host/include"
  "/opt/vc/userland/host_applications/linux/apps/raspicam"
  "/opt/vc/userland/host_applications/linux/libs/sm"
  "/opt/vc/userland"
  "../"
  "/opt/vc/include"
  "/opt/vc/include/interface/vcsm"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
