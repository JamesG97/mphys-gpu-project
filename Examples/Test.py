import OpenGL
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
#import sys
import numpy as np
import matplotlib.pyplot as plt

from PIL import Image

# Check that the imports have all succeeded
print("The imports do appear to have worked good sir.")

# matplotlib has also been installed, so saving figures can be done as follows 
X = [x/10 for x in range(0, 100)]
Y = [np.sin(X[i]) for i in range(0, 100)]
plt.plot(X, Y)
plt.savefig("hi")

# This is just a random comment

img = Image.open("hi.png").convert('RGB')
imgW = img.width
imgH = img.height
data = [0] * (imgW * imgH * 3)

def IX(x, y):
    return 3 * (x + (y * imgW))

for x in range(imgW):
    for y in range(imgH):
        r, g, b = img.getpixel((x, y))
        data[IX(x, y) + 0] = r
        data[IX(x, y) + 1] = g
        data[IX(x, y) + 2] = b

def loadTexture():
    # Generate a new texture from the image loaded
    glEnable(GL_TEXTURE_2D)
    texID = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texID)
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imgW, imgH, 0, GL_RGB, GL_UNSIGNED_BYTE, data)

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)

    return texID

# OpenGL Code is given below for a rectangle
def display():
    # Clear the color and depth buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glBegin(GL_QUADS)
    glTexCoord2f(0, 1)
    glVertex2f(-0.75, -0.75)
    glTexCoord2f(0, 0)
    glVertex2f(-0.75, 0.75)
    glTexCoord2f(1, 0)
    glVertex2f(0.75, 0.75)
    glTexCoord2f(1, 1)
    glVertex2f(0.75, -0.75)
    glEnd()

    # Copy the off-screen buffer to the screen.
    glutSwapBuffers()
    
def keyPressed(key, x, y):
    # Exit program if the esc key is pressed
    if (key == b'\x1b'):
        exit(0)
    
glutInit()
glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)
glutInitWindowSize(500, 500)
glutInitWindowPosition(200, 100)

wind = glutCreateWindow("The honorable gentleman's window")

loadTexture()

glutDisplayFunc(display)
glutKeyboardFunc(keyPressed)
glutMainLoop()