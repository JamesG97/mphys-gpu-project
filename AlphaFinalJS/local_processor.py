import numpy as np

class LocalProcessor:
    def __init__(self, res_x : int, res_y : int, base_image_1, base_image_2):
        
        self.phase_contrast_viewer = True
        self.brightfield_viewer = False
        self.led_1_on = False
        self.led_2_on = False

        self.res_x = res_x
        self.res_y = res_y

        # Set an instance of the base image Y data to be called locally
        self.base_image_1 = base_image_1
        self.base_image_2 = base_image_2

        # Arrays to hold the image 1 and 2 data to be manipulated
        self.normalised_image_1 = np.empty((self.res_y, self.res_x), dtype = np.float32)
        self.normalised_image_2 = np.empty((self.res_y, self.res_x), dtype = np.float32)
        
        # Numerator and denominator in the phbase contrast intensity equation 
        self.phase_equation_numerator = np.empty((self.res_y * self.res_x), dtype = np.float32)
        self.phase_equation_denominator = np.empty((self.res_y * self.res_x), dtype = np.float32)
        
        # Array for phase contrasted image, Y data only
        self.phase_image = np.empty((self.res_y, self.res_x), dtype = np.float32)
        
    def phase_contrast_image(self, image_1, image_2):
        """ Obtain a normalised phase contrast image from two input images taken 
            at different illumination angles """
        self.normalised_image_1 = np.divide(image_1, self.base_image_1)
        self.normalised_image_2 = np.divide(image_2, self.base_image_2)

        self.phase_equation_numerator = np.subtract(self.normalised_image_1, self.normalised_image_2)
        self.phase_equation_denominator = np.add(self.normalised_image_1, self.normalised_image_2)

        # Make sure there are never any zeros in the denominator
        self.phase_equation_denominator += (self.phase_equation_denominator == 0)
        
        self.phase_image = np.divide(self.phase_equation_numerator, self.phase_equation_denominator)

        # Change the range of values to [0, 1]
        self.phase_image = np.subtract(self.phase_image, np.amin(self.phase_image))
        self.phase_image = np.divide(self.phase_image, np.amax(self.phase_image))

        return self.phase_image
    
    def brightfield_image(self, image):
        ###### Convert from YUV to brightfield ######

        if self.led_1_on:
            ###### Set led 1 on and 2 off
        elif self.led_2_on:
            ###### Same
        



    @staticmethod
    def get_Y_data(raw_array, res_x, res_y):
        return  np.reshape(raw_array[:res_x * res_y].astype(np.float32), (res_y, res_x))
