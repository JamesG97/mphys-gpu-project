from picamera import mmal, exc
from picamera.mmalobj import to_rational
from picamera.array import PiRGBArray
import numpy as np
from time import sleep
import yaml
import io
from phase_processor import PhaseProcessor

class CameraController:

    # Property getters for the high and low resolution base images dependent on whether the
    # camera is in high or low res mode
    def _get_base_image_1(self):
        return self.base_image_1_high_res.getvalue() if self.high_res_mode else self.base_image_1_low_res.getvalue()
    base_image_1 = property(_get_base_image_1)
    
    def _get_base_image_1_stream(self):
        return self.base_image_1_high_res if self.high_res_mode else self.base_image_1_low_res
    base_image_1_stream = property(_get_base_image_1_stream)


    def _get_base_image_2(self):
        return self.base_image_2_high_res.getvalue() if self.high_res_mode else self.base_image_2_low_res.getvalue()
    base_image_2 = property(_get_base_image_2)

    def _get_base_image_2_stream(self):
        return self.base_image_2_high_res if self.high_res_mode else self.base_image_2_low_res
    base_image_2_stream = property(_get_base_image_2_stream)
    
    resolution = property(lambda self: (self.res_x, self.res_y))

    def __init__(self, camera, led_controls, allow_camera_to_settle = True, visible_camera_preview = True):
        
        # Initialise the instance with the LEDs off if the program did not clean up previously
        led_controls.leds_all_off()

        # Set up the local instance of camera and its resolution
        self.camera = camera
        self.toggle_high_res_mode(False)

        # Can set the exposure time to a minimal value for debugging the program
        self.settle_time = 1 if allow_camera_to_settle else 0.05

        # Set the variables to reset the camera analogue and digital gain
        self.MMAL_PARAMETER_ANALOG_GAIN = mmal.MMAL_PARAMETER_GROUP_CAMERA + 0x59
        self.MMAL_PARAMETER_DIGITAL_GAIN = mmal.MMAL_PARAMETER_GROUP_CAMERA + 0x5A

        # Fill dictionary to handle the camera settings
        led_controls.leds_all_on()
        self.camera_settings = self.auto_expose_to_white(show_preview = visible_camera_preview)
        led_controls.leds_all_off()
        self.set_camera_settings(self.camera_settings)


    """ Handle obtaining optimal camera settings for a given illumination """
    def auto_expose_to_white(self, show_preview, update_shutter_speed = True,  **kwargs):
        """ Freeze the settings after auto-exposing to white illumination.
            Kwargs for the rgb_image function """
        
        # Function required only in auto_expose_to_white
        def rgb_image(**kwargs):
            """Capture an image and return an RGB numpy array"""
            with PiRGBArray(self.camera) as output:
                self.camera.capture(output, format='rgb', **kwargs)
                return output.array

        # Begin auto exposing  
        print("Letting the camera auto-expose", end="")
        
        if show_preview:
            self.camera.start_preview(fullscreen=False, window=(0,50,640,480))
            
        for i in range(6):
            print(".", end="")
            sleep(self.settle_time)
        print("done")

        print("Freezing the camera settings...")
        self.camera.shutter_speed = self.camera.exposure_speed
        #print("Shutter speed = {}".format(self.camera.shutter_speed))
        self.camera.exposure_mode = "off"
        #print("Auto exposure disabled")
        g = self.camera.awb_gains
        self.camera.awb_mode = "off"
        self.camera.awb_gains = g
        #print("Auto white balance disabled, gains are {}".format(g))
        #print("Analogue gain: {}, Digital gain: {}".format(self.camera.analog_gain, self.camera.digital_gain))

        if (update_shutter_speed):
            print("Adjusting shutter speed to avoid saturation", end="")
            for i in range(3):
                print(".", end="")
                self.camera.shutter_speed = int(self.camera.shutter_speed * 230.0 / np.max(rgb_image()))
                sleep(2 * self.settle_time)
        
        if show_preview:
            self.camera.stop_preview()

        print("done") 
              
        return {k: getattr(self.camera, k) for k in ['analog_gain', 'digital_gain', 'shutter_speed', 'awb_gains', 'awb_mode', 'exposure_mode']}
    
    
    def save_settings(self, output="output/camera_settings.yaml"):
        """Save the camera settings to a YAML file"""
        camera_settings = {k: getattr(self.camera, k) for k in ['analog_gain', 'digital_gain', 'shutter_speed', 'awb_gains', 'awb_mode', 'exposure_mode']}
        with open(output, "w+") as outfile:
            yaml.dump(camera_settings, outfile)

    # Currently broken as you can't set the analog and digital gain attributes like this
    def restore_settings(self, filename, ignore=['analog_gain', 'digital_gain']):
        """Load camera settings from a YAML file"""
        with open(filename, "r") as infile:
            settings = yaml.load(infile, Loader=yaml.FullLoader)
            for k, v in settings.items():
                if k not in ignore:
                    setattr(self.camera, k, v)

    def set_camera_settings(self, camera_settings):
        """ Set the camera settings from a dictionary, also uses gain setting
            functions below. """
        self.camera.shutter_speed = camera_settings.get('shutter_speed')
        self.camera.awb_mode = camera_settings.get('awb_mode')
        self.camera.awb_gains = camera_settings.get('awb_gains')
        self.camera.exposure_mode = camera_settings.get('exposure_mode')
        self.set_analog_gain(camera_settings.get('analog_gain'))
        self.set_digital_gain(camera_settings.get('digital_gain'))

    def toggle_high_res_mode(self, state):
        self.high_res_mode = state
        
        if self.high_res_mode:
            self.res_x = 3280
            self.res_y = 2464
        else:
            self.res_x = 1280
            self.res_y = 720

        self.camera.resolution = (self.res_x, self.res_y)

    def get_base_images_local(self, led_controls):
        """ Take the base images without a sample which are used to normalise the sample 
            images. led_controls is a reference to the class which handles the led illumination """
        # Raw data of the base images for normalisation
        base_image_1_raw = np.empty((self.res_x * self.res_y * 3) << 2, dtype = np.uint8)
        base_image_2_raw = np.empty((self.res_x * self.res_y * 3) << 2, dtype = np.uint8)
        
        # Data of the base images to be returned
        base_image_1 = np.empty((self.res_x, self.res_y), dtype = np.float32)
        base_image_2 = np.empty((self.res_x, self.res_y), dtype = np.float32)

        # Take the two base images with the correct LED
        led_controls.is_led_1_on = True
        self.camera.capture(base_image_1_raw, 'yuv')
        led_controls.is_led_1_on = False

        led_controls.is_led_2_on = True
        self.camera.capture(base_image_2_raw, 'yuv')
        led_controls.is_led_2_on = False

        # Take only the Y data from the raw base images and cast as float type for processing
        ######### Float between 0.0 and 255.0 ###########
        base_image_1 =  PhaseProcessor.get_Y_data(base_image_1_raw, self.res_x, self.res_y)
        base_image_2 =  PhaseProcessor.get_Y_data(base_image_2_raw, self.res_x, self.res_y)

        # Make sure there are never any zeros in the denominator of the normalisation
        base_image_1 += (base_image_1 == 0)
        base_image_2 += (base_image_2 == 0)

        return base_image_1, base_image_2
        
    def get_base_images_web_gl(self, led_controls):
        """ 
        Take the base images without a sample which are used to normalise the sample 
        images. led_controls is a reference to the class which handles the led illumination.
        Takes both low resolution and high resolution images for normalisation in both instances
        """
        
        def capture_base_images():
            # Data of the base images for normalisation
            base_image_1 = io.BytesIO()
            base_image_2 = io.BytesIO()
            # Take the two base images with the correct LED
            led_controls.is_led_1_on = True
            led_controls.is_led_2_on = False
            self.camera.capture(base_image_1, 'jpeg')
            led_controls.is_led_1_on = False

            led_controls.is_led_1_on = False
            led_controls.is_led_2_on = True
            self.camera.capture(base_image_2, 'jpeg')
            led_controls.is_led_2_on = False

            return base_image_1, base_image_2
        
        self.toggle_high_res_mode(False)
        self.base_image_1_low_res, self.base_image_2_low_res = capture_base_images()
        self.toggle_high_res_mode(True)
        self.base_image_1_high_res, self.base_image_2_high_res = capture_base_images()
        self.toggle_high_res_mode(False)


    """ Handle the gain setting """
    def set_gain(self, gain, value):
        """
        Set the analog gain of a PiCamera.
        
        gain: either MMAL_PARAMETER_ANALOG_GAIN or MMAL_PARAMETER_DIGITAL_GAIN
        value: a numeric value that can be converted to a rational number.
        """
        if gain not in [self.MMAL_PARAMETER_ANALOG_GAIN, self.MMAL_PARAMETER_DIGITAL_GAIN]:
            raise ValueError("The gain parameter was not valid")
        ret = mmal.mmal_port_parameter_set_rational(self.camera._camera.control._port, 
                                                        gain,
                                                        to_rational(value))
        if ret == 4:
            raise exc.PiCameraMMALError(ret, "Are you running the latest version of the userland libraries? Gain setting was introduced in late 2017.")
        elif ret != 0:
            raise exc.PiCameraMMALError(ret)

    def set_analog_gain(self, value):
        """Set the gain of a PiCamera object to a given value."""
        self.set_gain(self.MMAL_PARAMETER_ANALOG_GAIN, value)
    
    def set_digital_gain(self, value):
        """Set the digital gain of a PiCamera object to a given value."""
        self.set_gain(self.MMAL_PARAMETER_DIGITAL_GAIN, value)