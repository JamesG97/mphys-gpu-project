import sys
import re

# Probably doesn't need its own class, but at least it's out of the way here
class ArgParser:
    def __init__(self, argv):
        ipPattern = "(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])"
        portPattern = "([0-9]+)"
        
        self.uses_webgl = False
        self.addr = "None"
        self.port = "None"
        
        for i in range(len(argv)):
            av = argv[i]
            
            if av == '-wgl':
                print("use webgl")
                
                self.uses_webgl = True
                
                i += 1
                if i >= len(argv) or re.match(ipPattern, argv[i]) == None:
                    self.addr = "None"
                else:
                    self.addr = argv[i]
                
                i += 1
                if i >= len(argv) or re.match(portPattern, argv[i]) == None:
                    self.port = "None"
                else:
                    self.port = argv[i]