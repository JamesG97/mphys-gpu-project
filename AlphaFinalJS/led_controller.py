import RPi.GPIO as gpio

class LEDController:
    def __init__(self, image_1_led_pin : int, image_2_led_pin : int):
        # Set the led pin data
        self._image_1_led_pin = [image_1_led_pin, False]
        self._image_2_led_pin = [image_2_led_pin, False]

        # Set up the gpio pins
        gpio.setmode(gpio.BCM)
        gpio.setup(self._image_1_led_pin[0], gpio.OUT)
        gpio.setup(self._image_2_led_pin[0], gpio.OUT)

        # Initialise with both LEDs off
        self.leds_all_off()


    # Setter and getter for image 1 and 2 LEDs to switch on and off
    # State is False when off and True when on (DUH)
    def _get_led_1_pin(self):
        return self._image_1_led_pin[0]
    
    def _get_led_1_state(self):
        return self._image_1_led_pin[1]
    
    def _set_led_1_state(self, state : bool):
        self._image_1_led_pin[1] = state
        gpio.output(self._image_1_led_pin[0], gpio.HIGH if state else gpio.LOW)
    
    is_led_1_on = property(_get_led_1_state, _set_led_1_state)
    led_1_pin = property(_get_led_1_pin)


    def _get_led_2_pin(self):
        return self._image_2_led_pin[0]
    
    def _get_led_2_state(self):
        return self._image_2_led_pin[1]

    def _set_led_2_state(self, state : bool):
        self._image_2_led_pin[1] = state
        gpio.output(self._image_2_led_pin[0], gpio.HIGH if state else gpio.LOW)
    
    is_led_2_on = property(_get_led_2_state, _set_led_2_state)
    led_2_pin = property(_get_led_2_pin)

    # Functionality functions
    def leds_all_on(self):
        """ Switch on both LEDs """
        self.is_led_1_on = True
        self.is_led_2_on = True

    def leds_all_off(self):
        """ Switch off both LEDs """
        self.is_led_1_on = False
        self.is_led_2_on = False
    
    # If we need it??
    def toggle_leds(self):
        """ Toggle the illuminated LED """
        self.is_led_1_on = not self.is_led_1_on 
        self.is_led_2_on = not self.is_led_2_on

    def clean_up(self):
        """ Clean up to be called at the end of the main function 
            to switch off LEDs and free GPIO pins """
        self.leds_all_off()
        gpio.cleanup()