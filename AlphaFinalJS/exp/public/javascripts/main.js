
var canvas;
var gl;
var socket;

var resX, resY;

var hasColour = true;
var isPhaseContrast = true;
var gain = 1.0;

var phaseContrastProgram;
var brightfieldProgram;

var projectionMatrix = mat4.fromValues(
    1, 0, 0, 0, 
    0, 1, 0, 0,
    0, 0, 0, 0,
    0, 0, 0, 1);

const numTextures = 2;
var images = [];
var textures = [];
var baseImages = [];
var baseTextures = [];

var dataStarted = false;
var stepSizeXY = 250;
var stepSizeZ = 75;
var zStackStepSize = 20;
var zStackStepCount = 10;


main();

function main() {
    setUpWebGL();
    setUpSocket();

    drawScene();
}

function setUpWebGL() {
    canvas = document.querySelector('#glcanvas');
    gl = canvas.getContext('webgl');
    // If we don't have a GL context, give up now
    if (!gl) {
        alert('Unable to initialize WebGL. Your browser or machine may not support it.');
        return;
    }

    gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
    gl.clearDepth(1.0);                 // Clear everything
    gl.enable(gl.DEPTH_TEST);           // Enable depth testing
    gl.depthFunc(gl.LEQUAL);            // Near things obscure far things


    // Initialize a shader program; this is where all the lighting
    // for the vertices and so forth is established.
    const phaseContrastShaderProgram = initShaderProgram(gl, vsSource, phaseContrastFSSource);
    const brightfieldShaderProgram = initShaderProgram(gl, vsSource, brightfieldFSSource);

    phaseContrastProgram = setUpShaderProgram(phaseContrastShaderProgram);
    brightfieldProgram = setUpShaderProgram(brightfieldShaderProgram);

    brightfieldProgram.addUniform(gl, "u_use_colour");
}


function setUpShaderProgram (shaderProgram) {
    var program = new ShaderProgram(shaderProgram);

    setUpAttributes();
    setUpUniforms();

    return program;

    // Add attributes
    function setUpAttributes(){
        program.addAttribute(gl, 'aVertexPosition');
        program.addAttribute(gl, 'aTexCoords');
        var buffers = initBuffers(program.programInfo, 'aVertexPosition');

        program.addBuffer('aVertexPosition', buffers.positionBuffer);
        program.addBuffer('aTexCoords', buffers.texCoordBuffer);
    }

    // Add uniforms
    function setUpUniforms() {
        // Add projection matrix
        program.addUniform(gl, 'uProjectionMatrix');
        program.addUniform(gl, 'u_gain');

        // Define each texture and an associated image whose onload event updates the texture
        for (i = 0; i < numTextures; i++) {
            textures[i] = new Texture(gl, ('u_texture'+i), i);
            images[i] = new Image(); 

            const id = i;
            images[id].onload = function(){
                textures[id].setImage(gl, images[id]);
                drawScene();
            }; 
        }
        // Add the uniform location to the program info for rendering
        for (t in textures) program.addUniform(gl, textures[t].uniformName);


        // Repeat above for base textures
        for (i = 0; i < numTextures; i++) {
            baseTextures[i] = new Texture(gl, ('u_basetexture'+i), (i+numTextures));
            baseImages[i] = new Image();

            const id = i;
            baseImages[id].onload = function(){
                baseTextures[id].setImage(gl, baseImages[id]);
                drawScene();
            };
        }
        for (t in baseTextures) program.addUniform(gl, baseTextures[t].uniformName);
    }
}

// Initialise the vertices of a quad for the vertex attribute, and initialise the corresponding texture coordinates to draw a texture to it
function initBuffers(programInfo, vertexAttribName) {
    function genPositionBuffer() {
        // Create a buffer for the square's positions.
        const positionBuffer = gl.createBuffer();

        // Select the positionBuffer as the one to apply buffer
        // operations to from here on out.
        gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

        // Now create an array of positions for the square.
        const positions = [
             1.0,  1.0,  0.0,
            -1.0,  1.0,  0.0,
             1.0, -1.0,  0.0,
            -1.0, -1.0,  0.0,
        ];

        // Now pass the list of positions into WebGL to build the
        // shape. We do this by creating a Float32Array from the
        // JavaScript array, then use it to fill the current buffer.
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

        return positionBuffer;
    }

    function genTextureCoordsBuffer() {
        var buffer = gl.createBuffer(); // Create a WebGL buffer object
        gl.bindBuffer(gl.ARRAY_BUFFER, buffer); // Set the target buffer object with the WebGL buffer object
        
        // Enable the vertex array in programInfo
        gl.enableVertexAttribArray(programInfo.attribLocations[vertexAttribName]);
         
        // We'll supply texcoords as floats. Specifies location and data format of the vertex array
        gl.vertexAttribPointer(programInfo.attribLocations[vertexAttribName], 2, gl.FLOAT, false, 0, 0);
         
        // Set Texcoords.
        const positions = [
             1.0,  0.0,
             0.0,  0.0,
             1.0,  1.0,
             0.0,  1.0,
        ];
        //Create and initialise the buffer object's data store. STATIC_DRAW: Modified once, used many times
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW); 

        return buffer;
    }

    return {
        positionBuffer: genPositionBuffer(),
        texCoordBuffer: genTextureCoordsBuffer(),
    };
}



function setUpSocket() {
    // Take an input buffer and convert the data to base 64
    function arrayBufferToBase64(buffer) {
        var binary = '';
        var bytes = [].slice.call(new Uint8Array(buffer));
        bytes.forEach((b) => binary += String.fromCharCode(b));
        return window.btoa(binary);
    };

    // Update the canvas resolution to match that of incoming data
    function updateCanvasResolution() {
        canvas.width = resX;
        canvas.height = resY;

        // Modify viewport to still take the full screen in displaying the images (may be skewed in visualiser but saves at correct resolution)
        gl.viewport(0, 0, canvas.width, canvas.height);

        drawScene();
    }

    socket = io.connect();

    // Request the base images when the socket is set up in case the python script is already running
    socket.emit('requestBaseImages');

    // Message received on server
    socket.on('new message', function(data){
        addMessage(data['msg']);
    });

    // When recieving image, convert to base 64 and set the variable which maps the image to a texture on loading
    socket.on('setImage0', function(data){
        var base64Flag = 'data:image/jpeg;base64,';
        var imageStr = arrayBufferToBase64(data);

        images[0].setAttribute('src', base64Flag + imageStr); 

        if (canvas.width != images[0].width || canvas.height != images[0].height) {
            [resX, resY] = [images[0].width, images[0].height];

            updateCanvasResolution();
        }
    });
    socket.on('setImage1', function(data){
        var base64Flag = 'data:image/jpeg;base64,';
        var imageStr = arrayBufferToBase64(data);

        images[1].setAttribute('src', base64Flag + imageStr);  

        if (canvas.width != images[1].width || canvas.height != images[1].height) {
            [resX, resY] = [images[1].width, images[1].height];

            updateCanvasResolution();
        }
    });

    // As above for base images
    socket.on('setBaseImage0', function(data){
        var base64Flag = 'data:image/jpeg;base64,';
        var imageStr = arrayBufferToBase64(data);
        
        baseImages[0].setAttribute('src', base64Flag + imageStr);

        setButtonsEnabled(true);
    });
    socket.on('setBaseImage1', function(data){
        var base64Flag = 'data:image/jpeg;base64,';
        var imageStr = arrayBufferToBase64(data);
        
        baseImages[1].setAttribute('src', base64Flag + imageStr);

        setButtonsEnabled(true);
    });
}

//
// Draw the scene.
//
function drawScene() {
    // Clear the canvas before we start drawing on it.
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Use the desired shader program
    var program = (isPhaseContrast ? phaseContrastProgram : brightfieldProgram);
    program.useProgram(gl);

    // Bind the buffers to the attributes for the vertex shader
    program.bindBufferToAttribute(gl, 'aVertexPosition', 3, gl.FLOAT, false, 0, 0);
    program.bindBufferToAttribute(gl, 'aTexCoords', 2, gl.FLOAT, false, 0, 0);

    // Set the shader uniforms: projection matrix, texture uniforms and base texture uniforms
    gl.uniformMatrix4fv( program.getUniformLocation('uProjectionMatrix'), false, projectionMatrix); 
    gl.uniform1f (program.getUniformLocation('u_gain'), gain);
    for (t in textures) gl.uniform1i (program.getUniformLocation(textures[t].uniformName), textures[t].textureIndex);
    for (t in baseTextures) gl.uniform1i (program.getUniformLocation(baseTextures[t].uniformName), baseTextures[t].textureIndex);

    // If brightfield set the is colour uniform
    if (!isPhaseContrast) gl.uniform1i (program.getUniformLocation('u_use_colour'), hasColour ? 1 : 0);

    // Draw the quad (and the texture over it)
    {
        const offset = 0;
        const vertexCount = 4;
        gl.drawArrays(gl.TRIANGLE_STRIP, offset, vertexCount);
    }
}

//---------------------------------------------------
//              Page functionality
//---------------------------------------------------
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function addMessage(msg) {
    var html = $('#Messages').html();
    html = msg + "<br>" + html;
    $('#Messages').html(html);
}

function setButtonsEnabled(state) {
    $(".viewerOptionButton").prop('disabled', !state);
    $(".optionButton").prop('disabled', !state);
    $(".movementButton").prop('disabled', !state);
}

function clearScreen() {
    for (t in textures) textures[t].setData(gl, new Uint8Array([255, 255, 255, 255]));
    for (t in baseTextures) baseTextures[t].setData(gl, new Uint8Array([255, 255, 255, 255]));
    drawScene();
}

function isRunning() {
    return ($("#pausePlay").text() == "Pause");
}
// Play/Pause
function toggleRunning() {
    setRunning(!isRunning());
}
function setRunning(newState) {
    currentState = isRunning();

    // Keep python state same as javascript state 
    if (newState != currentState) 
    {
        socket.emit('toggleStreamingState', newState);

        // Change the text on the webpage button depending on whether
        // the system is running
        $("#pausePlay").text(newState ? "Pause" : "Play");
    }
}

// End stream
async function endStream() {
    // Send the python disconnection request
    socket.emit("killPython");

    // Allow short amount of time for python to finish sending all data
    await sleep(500);

    // Update webpage elements to reflect that the stream has stopped
    addMessage("Ending the stream and killing the Python program.");
    clearScreen();
    setButtonsEnabled(false);
    $("#pausePlay").text("Pause")
}

// Save the displayed image
function saveImage(saveRaw) {
    var wasRunning = isRunning();
    setRunning(false);
    addMessage("Saving...");
    
    if (!saveRaw) {
        // Redraw and save the processed image
        drawScene();
        canvas.toBlob(function(blob) {
            saveAs(blob, "processed_image_" + (isPhaseContrast ? "PC" : (hasColour ? "BF_C" : "BF_GS")) + ".jpg");
        });
    } else {
        var zip = new JSZip();

        // Save the constituent images
        for (i = 0; i < numTextures; i++) {

            // Get image data in non base64 form
            var data = atob((images[i].getAttribute("src")).split(",")[1]);

            // Construct the blob
            var arraybuffer = new ArrayBuffer(data.length);
            var view = new Uint8Array(arraybuffer);
            for (var j=0; j<data.length; j++) {
                view[j] = data.charCodeAt(j) & 0xff;
            }
            var blob = new Blob([arraybuffer], {type: 'application/octet-stream'});

            // Save blob
            zip.file("raw_image_" + i + ".jpg", blob);
        }
        // Save the base image
        for (i = 0; i < numTextures; i++) {

            // Get image data in non base64 form
            var data = atob((baseImages[i].getAttribute("src")).split(",")[1]);

            // Construct the blob
            var arraybuffer = new ArrayBuffer(data.length);
            var view = new Uint8Array(arraybuffer);
            for (var j=0; j<data.length; j++) {
                view[j] = data.charCodeAt(j) & 0xff;
            }
            var blob = new Blob([arraybuffer], {type: 'application/octet-stream'});

            // Save blob
            zip.file("raw_base_image_" + i + ".jpg", blob);
        }

        zip.generateAsync({type:"blob"})
        .then(function(content) {
            saveAs(content, "rawData.zip");
        });
    }
    
    setRunning(wasRunning);
    addMessage("Image saved to downloads folder");
}

function resetExposure() {
    socket.emit("requestExposureReset");
}

function updateBaseImages() {
    socket.emit("requestBaseImageUpdate");

}

async function stepFullRes() {
    addMessage("Full Res has been called");
    setRunning(false);
    await sleep(500);
    socket.emit("requestFullResCapture");
}

function captureZStack() {
    addMessage("Capturing z stack and saving images");
    socket.emit("requestZStack", zStackStepCount, zStackStepSize);
}

function moveSample(x, y, z) {
    socket.emit("requestMoveSample", stepSizeXY * x, stepSizeXY * y, stepSizeZ * z);
}

function togglePhaseContrast() {
    isPhaseContrast = $('#togglePhaseContrast').prop('checked');

    if (isPhaseContrast) $('h1').text("Phase Contrast Microscope Display");
    else $('h1').text("Brightfield Microscope Display");

    refreshTab();

    drawScene();
}
function toggleColour() {
    hasColour = !hasColour;

    drawScene();
}
function setGain(value) {
    gain = value;

    drawScene();
}