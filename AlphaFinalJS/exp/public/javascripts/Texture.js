// Texture object keeps track of data used in opengl, and performs some basic opengl operations
class Texture {
	constructor(gl, uniformName, textureIndex) {
		this.uniformName = uniformName;		// Name of texture in the shader
		this.textureIndex = textureIndex;	// gl.TEXTURE + textureIndex represents the texture block in memory
		this.texture = gl.createTexture();	// Create a texture object to take the image and bind it to a texture unit

		// Set default data
		this.setData(gl, new Uint8Array([255, 255, 255, 255]));
	}

	setActiveTexture(gl) {
	    gl.activeTexture(gl.TEXTURE0 + this.textureIndex);	// Set the active texture unit
	}

	bindToActiveTexture(gl) {    
	    gl.bindTexture(gl.TEXTURE_2D, this.texture);		// Bind the base image texture to the active texture unit
	}

	// Set data from an array
	setData(gl, data) {
		// Select this texture's unit as active and bind this texture
		this.setActiveTexture(gl);
		this.bindToActiveTexture(gl);

		// Load the texture data into the currently bound texture object
	    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, data);

        this.setDefaultParameters(gl);
	}

	// Set data from a javascript image object
	setImage(gl, image) {
		this.setActiveTexture(gl);
		this.bindToActiveTexture(gl);

		// Load the image into the currently bound texture object
	    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

        this.setDefaultParameters(gl);
	}

	// Set texture parameters to some reasonable values, can be overwriten after setting data anyway
	setDefaultParameters(gl) {
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
	}
}
