class ShaderProgram {
    constructor(shaderProgram) {
        this.shaderProgram = shaderProgram;

        this.programInfo = {
            attribLocations: {}, 
            uniformLocations: {}, 
        };

        this.buffers = {};
        
    }


    addAttribute (gl, attribName) {
        var attribLocation = gl.getAttribLocation(this.shaderProgram, attribName);
        this.programInfo.attribLocations[attribName] = attribLocation;
    }
    getAttributeLocation(attribName) {
        return this.programInfo.attribLocations[attribName];
    }


    addBuffer (attribName, buffer) {
        this.buffers[attribName] = buffer;
    }
    getBuffer (attribName) {
        return this.buffers[attribName];
    }
    bindBufferToAttribute (gl, attribName, numComponents, type, normalize, stride, offset) {
        gl.bindBuffer(gl.ARRAY_BUFFER, this.getBuffer(attribName));
        gl.vertexAttribPointer(this.getAttributeLocation(attribName), numComponents, type, normalize, stride, offset); 
        gl.enableVertexAttribArray(this.getAttributeLocation(attribName));
    }


    addUniform (gl, uniformName) {
        var uniformLocation = gl.getUniformLocation(this.shaderProgram, uniformName);
        this.programInfo.uniformLocations[uniformName] = uniformLocation;
    }
    getUniformLocation(uniformName) {
        return this.programInfo.uniformLocations[uniformName];
    }


    useProgram(gl) {
        gl.useProgram(this.shaderProgram);
    }
}