$('document').ready(function(){
	$('input[type=range]').on('input', function () {
	    $(this).trigger('change');
	});

	// Callback setGain when gain is changed
	$('#gainSlider').change( function() { 
        setGain($('#gainSlider').val());

        // Update text on label
        $('#gainSliderValue').text(parseFloat($('#gainSlider').val()).toFixed(2));
	});

	// Callback setGain when gain is changed
	$('#stepSizeSliderXY').change( function() { 
		stepSizeXY = parseInt($('#stepSizeSliderXY').val());

        // Update text on label
        $('#stepSizeValueXY').text(stepSizeXY);
	});

	// Callback setGain when gain is changed
	$('#stepSizeSliderZ').change( function() { 
		stepSizeZ = parseInt($('#stepSizeSliderZ').val());

        // Update text on label
        $('#stepSizeValueZ').text(stepSizeZ);
	});

	// Callback setGain when gain is changed
	$('#zStackCountSlider').change( function() { 
		zStackStepCount = parseInt($('#zStackCountSlider').val());

        // Update text on label
        $('#zStackCountValue').text(zStackStepCount);
	});

	// Callback setGain when gain is changed
	$('#zStackStepSizeSlider').change( function() { 
		zStackStepSize = parseInt($('#zStackStepSizeSlider').val());

        // Update text on label
        $('#zStackStepSizeValue').text(zStackStepSize);
	});
});


function openTab (tabName) {
	// Hide all tabs and open relevant options / console tab
	$('.tab').hide();
	if (tabName == "consoleTab") $('#consoleTab').show();
	else if (tabName == "movementTab") $('#movementTab').show();
	else {
		$('#optionsTab').show();
		$('#optionsTabShared').show();

		if ($('#togglePhaseContrast').prop('checked') == true) $('#optionsTabPhaseContrast').show();
		else $('#optionsTabBrightfield').show();
	}

	$('.tabOptionButton').removeClass("buttonSelected");
	$('#'+tabName+"Button").addClass("buttonSelected");
}

function refreshTab() {
	// If options tab is open
	if(! $('#optionsTab').is(':hidden')) {
		// Hide all tabs
		$('.tab').hide();

		// Reopen the options tab and shared section
		$('#optionsTab').show();
		$('#optionsTabShared').show();

		// Reopen relevent phase contrast / brightfield section
		if ($('#togglePhaseContrast').prop('checked') == true) $('#optionsTabPhaseContrast').show();
		else $('#optionsTabBrightfield').show();
	}
}

function toggleOptionsMenu() {

	var transitionTime = 500;

	// Get the default resolution from css
	var resX = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--res-x-default'), 10);
	var resY = parseInt(getComputedStyle(document.documentElement).getPropertyValue('--res-y-default'), 10);
	var lineColourDark = getComputedStyle(document.documentElement).getPropertyValue('--line-colour-dark');

	console.log(lineColourDark);

	// Slide the options menu in or out depending on its current state
	if ($('#options').is(':hidden')) {

		var newOptionsWidth = $('#viewer').parent().width() - (resX + 30);

		$('#viewer').animate({
			width: resX,
			height: resY,
		}, transitionTime);

		$('#options').show();
		$('#options').animate({
			width: newOptionsWidth,
			height: resY,
		}, transitionTime);

		$('#optionsHider').animate({
			height: resY,
		}, transitionTime, function(){
			$(this).css("border-left-width", "0px");
			$(this).css("border-right-width", "1px");
			$(this).find('button').text("»");
		});


	} else {

		var newViewerWidth = $('#viewer').parent().width() - 30;
		var newHeight = (newViewerWidth * (9.0 / 16.0)) + 30;

		$('#options').animate({
			width: 0,
			height: newHeight,
		}, transitionTime, function() {
			$(this).hide();
		});

		$('#viewer').animate({
			width: newViewerWidth,
			height: newHeight,
		}, transitionTime);

		$('#optionsHider').animate({
			height: newHeight,
		}, transitionTime, function(){
			$(this).css("border-left-width", "1px");
			$(this).css("border-right-width", "0px");
			$(this).find('button').text("«");
		});
	}

}
