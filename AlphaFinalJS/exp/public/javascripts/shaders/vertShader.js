
// Vertex shader program
const vsSource = `

attribute vec4 aVertexPosition;
attribute vec2 aTexCoords;

uniform mat4 uProjectionMatrix;

varying vec2 v_texcoord;

void main() {
	gl_Position = uProjectionMatrix * aVertexPosition;

	v_texcoord = aTexCoords;
}

`;