
// Phase contrast Fragment shader program
const phaseContrastFSSource = `
precision mediump float;

// Passed in from the vertex shader.
varying vec2 v_texcoord;
 
// The textures and base images
uniform sampler2D u_basetexture0;
uniform sampler2D u_basetexture1;
uniform sampler2D u_texture0;
uniform sampler2D u_texture1;

uniform float u_gain;


float luminosity(vec3 colour)
{
    return (0.299 * colour.r) + (0.587 * colour.g) + (0.114 * colour.b);
}

void main() {
    // Get base luminosity
    float base_luminosity_0 = luminosity(texture2D(u_basetexture0, v_texcoord).rgb);
    float base_luminosity_1 = luminosity(texture2D(u_basetexture1, v_texcoord).rgb);
    
    // Get luminosity
    float luminosity_0 = luminosity(texture2D(u_texture0, v_texcoord).rgb) / base_luminosity_0;
    float luminosity_1 = luminosity(texture2D(u_texture1, v_texcoord).rgb) / base_luminosity_1;

    // Approximate phase contrast
    float phase_contrast_luminosity = u_gain * ((luminosity_1 - luminosity_0) / (luminosity_0 + luminosity_1));

    // Map [-1, 1] => [0, 1]
    phase_contrast_luminosity = ((phase_contrast_luminosity) + 1.0) / 2.0;

    // Return the frag colour
    gl_FragColor = vec4(vec3(phase_contrast_luminosity), 1);
}


`;


// Brightfield Fragment shader program
const brightfieldFSSource = `
precision mediump float;

// Passed in from the vertex shader.
varying vec2 v_texcoord;
 
// The textures and base images
uniform sampler2D u_basetexture0;
uniform sampler2D u_basetexture1;
uniform sampler2D u_texture0;
uniform sampler2D u_texture1;

uniform float u_gain;
uniform int u_use_colour;

float luminosity(vec3 colour)
{
    return (0.299 * colour.r) + (0.587 * colour.g) + (0.114 * colour.b);
}

void main() {
    // Get base luminosity
    vec3 base_image_0 = texture2D(u_basetexture0, v_texcoord).rgb;
    vec3 base_image_1 = texture2D(u_basetexture1, v_texcoord).rgb;
    
    vec3 image_0 = texture2D(u_texture0, v_texcoord).rgb;
    vec3 image_1 = texture2D(u_texture1, v_texcoord).rgb;

    vec3 image_0_normalised = 0.5 * vec3(image_0.r / base_image_0.r, image_0.g / base_image_0.g, image_0.b / base_image_0.b);
    image_0_normalised = (u_gain * (image_0_normalised - 1.0)) + 1.0;
    vec3 image_1_normalised = 0.5 * vec3(image_1.r / base_image_1.r, image_1.g / base_image_1.g, image_1.b / base_image_1.b);
    image_1_normalised = (u_gain * (image_1_normalised - 1.0)) + 1.0;
    
    vec3 brightfield_image = (image_0_normalised.rgb + image_1_normalised.rgb) / 2.0;

    // Return the frag colour
    if (u_use_colour == 1) gl_FragColor = vec4(brightfield_image, 1);
    else gl_FragColor = vec4(vec3(luminosity(brightfield_image)), 1);  // Greyscale
}


`;
