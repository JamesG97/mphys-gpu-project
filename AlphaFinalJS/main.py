from camera_controller import CameraController
from led_controller import LEDController
from phase_processor import PhaseProcessor
from socket_controller import SocketController
from argument_parser import ArgParser
from picamera import PiCamera
import cv2
import numpy as np
from time import sleep
from datetime import datetime
import io
import os
import re
import sys
from sangaboard import Sangaboard
from PIL import Image
import time


##########################################
######### WebGL button functions #########
##########################################

def reset_base_images_webgl(camera_controls, led_controls, socket_controls):
    # Function to retake the base images when the button is pressed on the webpage
    camera_controls.get_base_images_web_gl(led_controls)
    socket_controls.set_base_images()

def set_camera_settings(camera_controls, led_controls, socket_controls):
    # Function to calibrate the camera settings when the button is pressed on the webpage
    socket_controls.loop_paused = True
    sleep(0.1)

    led_controls.leds_all_on()
    camera_controls.camera_settings = camera_controls.auto_expose_to_white(False, update_shutter_speed=False)
    led_controls.leds_all_off()
    camera_controls.set_camera_settings(camera_controls.camera_settings)

    sleep(0.1)
    socket_controls.loop_paused = False

def step_full_res(camera_controls, socket_controls, state):
    # Magic freeze and enhance function when the button is pressed on the webpage
    # State is True or False to switch on and off high resolution respectively
    socket_controls.loop_paused = True
    socket_controls.high_res_mode = state

    # 4 because the sending is 2 images behind (to be fixed if time allows)
    socket_controls.high_res_number = 4 if state else 0

    # Toggle camera to desired mode (high res / low res) and send correct base images to javascript
    camera_controls.toggle_high_res_mode(state)
    sleep(1)

    # Unpause
    socket_controls.loop_paused = False

def capture_z_stack(camera_controls, led_controls, socket_controls): 
    """
    If z stack has been called, move the zoom out of focus and iteratively capture 
    images while stepping the z value (zooming in). When 11 images have been taken
    (5 each side and the in focus image) reset the loop to normal running mode.
    """

    def image(led_id):
        led_controls.is_led_1_on = led_id == 1
        led_controls.is_led_2_on = led_id == 2

        camera_controls.camera.capture(
            "z_stacks/stack" + str(folderNumber) + "/z_stack_image_{0:03d}_{1:d}.jpg".format(socket_controls.z_stack_number, led_id), 
            format='jpeg', 
            quality=95, 
            use_video_port=True)
        sleep(0.1)

    # Set the conditions in the main loop to sequentially take 10 images
    # at different zoom levels
    socket_controls.loop_paused = True
    socket_controls.capturing_z_stack = True
    camera_controls.toggle_high_res_mode(True)
    sleep(0.5)
    
    if socket_controls.z_stack_step_size != 0:
        # Start out of focus, move slightly too far up then back down to ensure all backlash is taken up and the size of each step is constant
        socket_controls.move_sample(0, 0, -(socket_controls.z_stack_step_size * int(socket_controls.z_stack_number / 2)) - 100)
        sleep(0.5)
        socket_controls.move_sample(0, 0, 100)

    # Get the number 1 bigger than the largest folder index number
    folderNumber = 0
    for d in os.listdir(os.getcwd() + "/z_stacks"):
        # if no file type, treat as a folder
        if '.' not in d:
            # Search for 1 or more digits at end of string d
            num = re.search('\d+$', d)
            num = int(num.group()) if num else 0
            if num > folderNumber:
                folderNumber = num
    # Make the new directory for the z stack
    folderNumber += 1
    os.mkdir("z_stacks/stack" + str(folderNumber))

    # Save the base images
    base_im_1 = Image.open(camera_controls.base_image_1_stream)
    base_im_2 = Image.open(camera_controls.base_image_2_stream)
    base_im_1.save("z_stacks/stack" + str(folderNumber) + "/base_1.bmp")
    base_im_2.save("z_stacks/stack" + str(folderNumber) + "/base_2.bmp")

    # For each step in the z stack
    while socket_controls.z_stack_number >= 0:
        # Image with both led configurations
        image(1)
        image(2)
        
        if socket_controls.z_stack_step_size != 0:
            # Iteratively move through z locations
            socket_controls.move_sample(0, 0, socket_controls.z_stack_step_size)
            sleep(0.5)

        # Cycle through required images
        socket_controls.z_stack_number -= 1
    
    # Return to the in focus location
    led_controls.leds_all_off()
    socket_controls.capturing_z_stack = False
    if socket_controls.z_stack_step_size != 0:
        socket_controls.move_sample(0, 0, -socket_controls.z_stack_step_size * int(socket_controls.z_stack_number / 2))

    camera_controls.toggle_high_res_mode(False)
    sleep(0.5)
    socket_controls.loop_paused = False

def move_sample(sangaboard_controls, x = 0, y = 0, z = 0):
    # Function to move the motors in the x, y and z directions where z is the zoom
    sangaboard_controls.move_rel([x, y, z])

"""
def local_main(led_controls, camera_controls):

    ######################################
    ####### Currently discontinued #######
    ######################################

    # Get base images
    base_image_1, base_image_2 = camera_controls.get_base_images_local(led_controls)
    image_1 = np.ones((res_y, res_x), dtype = np.float32)
    image_2 = np.ones((res_y, res_x), dtype = np.float32)
    image_raw = np.empty((res_x * res_y * 3) << 2, dtype = np.uint8)
    phase_image = np.empty((res_y, res_x), dtype = np.float32)
    
    # Set up objects required for image manipulation and display
    image_processor = PhaseProcessor(res_x, res_y, base_image_1, base_image_2)
    cv2.namedWindow("frame", cv2.WINDOW_NORMAL)

    # Flip between LEDs, 0 for image 1, 1 for image 2
    counter = 0
    # Pause functionality
    loop_paused = False
    led_controls.is_led_1_on = True
    led_controls.is_led_2_on = False
    for f in camera_controls.camera.capture_continuous(image_raw, format='yuv', use_video_port=True):
        if not loop_paused:
            if counter == 0:
                # When image 1 has been taken, switch off led 1, switch on led 
                # 2 for the next frame which is image 2
                led_controls.is_led_1_on = False
                led_controls.is_led_2_on = True
                image_1 = PhaseProcessor.get_Y_data(image_raw, res_x, res_y)
            elif counter == 1:
                # As above, but for image 2
                led_controls.is_led_1_on = True
                led_controls.is_led_2_on = False
                image_2 = PhaseProcessor.get_Y_data(image_raw, res_x, res_y)

            phase_image = image_processor.phase_contrast_image(image_1, image_2)

            # Flip between 0 and 1
            counter = (counter + 1) % 2

        cv2.imshow('frame', phase_image)

        # Handle keypresses, checks for keypress every 1ms, 0xFF handles 64 bit system
        key = cv2.waitKey(1) & 0xFF

        if key == 27:
            # 27 == esc
            print("Exiting loop.")
            break
        elif key == ord('p'):
            print("Paused")
            loop_paused = True
        elif key == ord('r'):
            print("Resuming")
            loop_paused = False
        elif key == ord('s'):
            print("Saving image")
            saved_image = np.multiply(phase_image, 255).astype(np.uint8)
            cv2.imwrite(str(datetime.now()) + ".bmp", saved_image)
        elif key == ord('b'):
            print("Both LEDs on")
            led_controls.leds_all_on()
        elif key == ord('e'):
            print("Re-setting the camera settings")
            camera_controls.camera_settings = camera_controls.auto_expose_to_white(False)

        # # Is this the best way to do it??
        # elif key == ord('h'):
        #     if not high_res_mode:
        #         print("Entering high resolution mode")
        #     elif high_res_mode:
        #         print("Leaving high resolution mode")
        #     high_res_mode = not high_res_mode
        # elif key == ord('1'):
        #     print("Entering phase contrast mode")
        #     if not phase_contrast_viewer:
        #         phase_contrast_viewer =  True
        #         brightfield_viewer = False
        # elif key == ord('2'):
        #     print("Entering brightfield mode")
        #     if not brightfield_viewer:
        #         phase_contrast_viewer = False
        #         brightfield_viewer =  True
        # elif key == ord('3'):
        #     print("Entering brightfield mode with LED 1 on")
        #     if not brightfield_viewer:
        #         phase_contrast_viewer = False
        #         brightfield_viewer =  True
        #     if not led_1_on:
        #         led_1_on = True
        #         led_2_on = False
        # elif key == ord('4'):
        #     print("Entering brightfield mode with LED 2 on")
        #     if not brightfield_viewer:
        #         phase_contrast_viewer = False
        #         brightfield_viewer =  True
        #     if not led_2_on:
        #         led_1_on = False
        #         led_2_on = True
        
    
    # Local specific cleanup 
    cv2.destroyAllWindows()
"""

def web_gl_main(led_controls, camera_controls, sangaboard_controls, port='3000', addr='127.0.0.1'):
    """
    Function which is called when the argument -wgl is passed to the process. Generates the
    images to be sent to the WebGL canvas. Processing occurs in the JS shaders.
    """
    
    # Setting up a socket to the server
    socket_controls = SocketController(addr, port)
    # Send the button functionality functions to the socket controller
    socket_controls.get_functions_from_main(
        step_full_res, set_camera_settings, reset_base_images_webgl, move_sample, capture_z_stack,
        camera_controls, led_controls, sangaboard_controls)

    # Take base images
    socket_controls.send_message("Taking the base images.")
    camera_controls.toggle_high_res_mode(False)
    camera_controls.get_base_images_web_gl(led_controls)
    
    # Send base images to client
    socket_controls.send_message("Sending base images...")
    socket_controls.set_base_images()
    socket_controls.send_message("Base images sent.")

    # Set up a stream to capture to
    image_stream = io.BytesIO()
    fps = []
    previous_time = time.time()
    # Counter to handle flipping between LEDs
    counter = 0
    just_finished_high_res_captures = False
    while socket_controls.is_running:
        if not socket_controls.loop_paused:
            # Normal running mode
            current_time = time.time()
            if len(fps)<1000:
                fps.append(np.divide(1, np.subtract(current_time, previous_time)))
            elif len(fps) == 1000: 
                #print(fps)
                print(np.mean(fps[1:len(fps)]))
                print(np.std(fps[1:len(fps)], ddof=1))
                fps.append(0)
            else: continue
            previous_time = current_time
            camera_controls.camera.capture(image_stream, 
                format='jpeg', 
                quality=20 if not socket_controls.high_res_mode else 95, 
                use_video_port=True)
            
            # Truncate the BytesIO stream to current position
            # and go to the start
            image_stream.truncate()
            image_stream.seek(0)

            # Toggle between image 1 and 2
            if counter == 0:
                # When image 1 has been taken, switch off led 1, switch on led 2
                led_controls.is_led_1_on = False
                led_controls.is_led_2_on = True
                socket_controls.send_image_0(image_stream.getvalue()) 
            elif counter == 1:
                # As above, but for image 2
                led_controls.is_led_1_on = True
                led_controls.is_led_2_on = False
                socket_controls.send_image_1(image_stream.getvalue())  
            
            # Empty the stream to ensure no data is left over
            image_stream.truncate()

            # keep track of whether on image 1 or 2
            counter = (counter + 1) % 2
            if socket_controls.high_res_mode:
                # Reduce counter to take required number of images
                socket_controls.high_res_number -= 1

                if socket_controls.high_res_number == 0:
                    socket_controls.send_message("Finished taking high res images")
                    socket_controls.full_res_function(False)
                    socket_controls.loop_paused = True
                    just_finished_high_res_captures = True

            elif just_finished_high_res_captures:
                socket_controls.set_base_images()
                just_finished_high_res_captures = False
        # End if
    
        # Give break to send image 
        sleep(0.01)

    # End while

    # Disconnect the socket if Python falls out of the loop
    # without stop button being pushed on WebGL or initial
    # disconnect fails
    socket_controls.send_disconnection_request()


def main():
    # Set up variables and objects used in both local and webgl accelerated versions of the code
    sangaboard_controls = Sangaboard()
    led_controls = LEDController(23, 24)
    camera = PiCamera()
    camera_controls = CameraController(camera, led_controls, allow_camera_to_settle=True, visible_camera_preview=False)
    

    args_parser = ArgParser(sys.argv)

    if not args_parser.uses_webgl:
        """local_main(led_controls, camera_controls)"""
        print("use the proper version (-wgl)")

    else:   # Stream and do image processing on webgl client
        if args_parser.port == "None" and args_parser.addr == "None":
            print("using default address and port (localhost:3000)")
            web_gl_main(led_controls, camera_controls, sangaboard_controls)
        else:
            web_gl_main(led_controls, camera_controls, sangaboard_controls, args_parser.port, args_parser.addr)

       
    # General cleanup operations called when the window is closed or when
    # the socket has been disconnected to the server (note the server is not 
    # closed on the Python side)
    led_controls.clean_up()
    camera.close()

if __name__ == "__main__":
    main()