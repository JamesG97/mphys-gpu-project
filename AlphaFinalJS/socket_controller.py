import socketio
import io
import os
from time import sleep

class SocketController:
    """ Class which sets up a python side socket to talk with the nodejs
        server set up in 'www' and app.js. Contains the functionlity to
        send base images, receive requests to send base images and send
        the illuminated images """

    def __init__(self, ip='localhost', port='3000'):
        self.sio = socketio.Client()
        # Run state variables
        self.is_running = True
        self.loop_paused = False
        self.high_res_mode = False
        self.high_res_number = 0
        self.capturing_z_stack = False
        self.z_stack_number = -1
        self.z_stack_step_size = -1
               
        ## Callbacks from server events
        # Announce when connection established
        @self.sio.on('connect')
        def socket_connected():
            print("Connected as " + self.sio.eio.sid)

        # Announce when the socket is disconnected
        @self.sio.on('disconnect')
        def socket_disconnected():
            print("Disconnected")

        # Disconnect on a kill event
        @self.sio.on("kill")
        def kill_connection():
            self.sio.disconnect()
            # If disconnect fails, ends Python main loop
            self.is_running = False
        
        # Set the base images on the JS side when they're requested
        @self.sio.on("baseImagesRequested")
        def base_images_requested():
            """ Re-send base images to the server if required """
            self.set_base_images()
        
        # Toggle whether the LEDs and images are changing/being
        # sent when the play/pause button is pressed
        @self.sio.on("toggleLoop")
        def toggle_loop_paused(state):
            self.loop_paused = not state

        # Reset the camera settings when requested
        @self.sio.on("exposureResetRequested")
        def reset_exposure():
            self.reset_camera_settings()

        # Retake the base images if requested
        @self.sio.on("updateBaseImagesRequested")
        def update_base_images():
            # Pause to keep the counter consistent
            self.loop_paused = True
            self.retake_base_images()
            self.loop_paused = False

        # Freeze and enhance button, take high resolution images and pause
        @self.sio.on("fullResCaptureRequested")
        def capture_full_res():
            self.full_res_function(True)
            self.set_base_images()

        # Capture a z stack
        @self.sio.on("zStackRequested")
        def z_stack_function(step_count, step_size):
            self.z_stack_number = step_count
            self.z_stack_step_size = step_size
            self.capture_z_stack()

        # Move the sample relative to the objective
        @self.sio.on("moveSample")
        def move_sample_callback(x, y, z):
            self.move_sample(x, y, z)

        

        # Connect the socket to the required location
        self.sio.connect('http://%s:%s' % (ip, port))

    def set_base_images(self):
        """ Gets appropriate base image from camera controls """
        self.sio.emit("baseImage0", self.camera_controls.base_image_1)
        self.sio.emit("baseImage1", self.camera_controls.base_image_2)

    def send_disconnection_request(self):
        self.sio.emit('killPython')

    # Image from LED 1
    def send_image_0(self, image):
        self.sio.emit("newImage0", image)

    # Image from LED 2
    def send_image_1(self, image):
        self.sio.emit("newImage1", image)
        
    def send_message(self, message):
        self.sio.emit("message", message)

    # Acquire the button functionality functions from the main file with the given instances of each class
    def get_functions_from_main(self, 
            full_res_function, reset_camera_settings, retake_base_images, move_sample, capture_z_stack,
            camera_controls, led_controls, sangaboard_controls):

        self.camera_controls = camera_controls
        self.led_controls = led_controls
        self.sangaboard_controls = sangaboard_controls

        self.full_res_function = lambda state: full_res_function(self.camera_controls, self, state)
        self.reset_camera_settings = lambda: reset_camera_settings(self.camera_controls, self.led_controls, self)
        self.retake_base_images = lambda: retake_base_images(self.camera_controls, self.led_controls, self)
        self.move_sample = lambda x, y, z: move_sample(self.sangaboard_controls, x, y, z)
        self.capture_z_stack = lambda: capture_z_stack(self.camera_controls, self.led_controls, self)