"""
Main phase contrast file, with the phase contrast image function, the assorted 
OpenGL window and loop functions and the main function which runs it all
"""

import RPi.GPIO as gpio
import numpy as np
from time import sleep
from PIL import Image
from datetime import datetime
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
from State import *
from cameraSettings import *
from Gain import *
from Socket import * 

# Set up the 2 dependent classes which are required in the cameraState functions
setup = cameraSettings()
gain = Gain()
# Create an instance of the camera's State class
cameraState = State(1280, 960)

# Create an instance of the Socket class (connected to localhost:3000)
socket = Socket(ip='127.0.0.1', port='3000')

# Function to take two images, go through phase contrast maths and get the phase contrasted image
def phaseContrastImage(state):
       
    # Debug printout offset (get to middle of arrays)
    #offset = int((State.ResX*State.ResY)/2)
    
#    print("Camera 1:\n")
#    state.printCameraSettings()

    
    imData = io.BytesIO()
    state.camera.capture(imData, format='jpeg', use_video_port=True)
    socket.sendImage(imData.getvalue(), state.ResX, state.ResY)
    
    
    
    
    return
    # Add back in when working, but modify to capture jpeg
    
    # Take image 1
    cameraSettings.switch_to_settings(setup.camSettings1)
    gpio.output(18, gpio.HIGH)
    sleep(state.sleepTime)
    state.camera.capture(state.im1Raw, 'YUV', use_video_port=True)
    gpio.output(18, gpio.LOW)
        
    # Maybe take alternating images on alternating calls to phaseContrastImage
    # Take Image 2
    cameraSettings.switch_to_settings(setup.camSettings2)
    gpio.output(14, gpio.HIGH)
    sleep(state.sleepTime)
    state.camera.capture(state.im2Raw, 'YUV', use_video_port=True)
    gpio.output(14, gpio.LOW)
    
    
    
    return
    # Further processing will not be needed here
    #   |
    #   V
    
    
    
    # Get the Y data from the images
    state.im1 = state.im1Raw[:state.ResY * state.ResX]
    state.im2 = state.im2Raw[:state.ResY * state.ResX]
    
    # Divide by base images to begin normalisation
    state.intermediate1 = np.divide(state.im1.astype(np.float32), state.im1Base.astype(np.float32))
    state.intermediate2 = np.divide(state.im2.astype(np.float32), state.im2Base.astype(np.float32))

    # Perform phase contrast maths; I = (I_1 - I_2) / (I_1 + I_2)
    state.Num = np.subtract(state.intermediate1, state.intermediate2)
    state.Denom = np.add(state.intermediate1, state.intermediate2)
    
    # Handle any zeros in the denominator by setting them to 1 which is assumed to be OK as the range is [0, 255]
    state.Denom += (state.Denom == 0)
    state.im = np.divide(state.Num, state.Denom)
    
    # Normalise to the range [0, 1] by shifting all values to begin at 0 then dividing by the largest number
    state.im = np.subtract(state.im, np.amin(state.im))
    state.im = np.divide(state.im, np.amax(state.im))

def keyPressed(key, x, y):
    # Exit program if the esc key is pressed
    if (key == b'\x1b'):
        print("Exiting...")
        glutLeaveMainLoop()
    # Resume when paused
    if (key == b'r'):
        print("Resume")
        cameraState.start = True
    # Start loop when sample is placed in
    if (key == b's'):
        print("Saving phase contrast image")
        image = Image.fromarray(np.multiply(cameraState.im.reshape((cameraState.ResY, cameraState.ResX)), 255).astype(np.uint8), 'L')
        image.save(str(datetime.now()), 'png')
    # Pause loop
    if (key == b'p'):
        print("Paused")
        cameraState.start = False

def loadTexture(Data, imgW, imgH, gl_type = GL_UNSIGNED_BYTE):
    # Generate a new texture from the image data
    glEnable(GL_TEXTURE_2D)
    texID = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, texID)
    
    # GL_LUMINANCE is greyscale for phase contrast images, GL_TYPE handles whether the data is unsigned bytes (im1 and 2) or floats (final image)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imgW, imgH, 0, GL_LUMINANCE, gl_type, Data)

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)

    return texID

def display():
    # Clear the color and depth buffers
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    
    # Function to create a section in the window and load a given data set to it
    def imageTexture(texture, ResX, ResY, x1, x2, gl_type = GL_UNSIGNED_BYTE):
        texID = loadTexture(texture, ResX, ResY, gl_type)
        
        # Create a rectangular section in the window, y range [-1,1], x range given as argument
        glBegin(GL_QUADS)
        glTexCoord2f(0, 1)
        glVertex2f(x1, -1)
        glTexCoord2f(0, 0)
        glVertex2f(x1, 1)
        glTexCoord2f(1, 0)
        glVertex2f(x2, 1)
        glTexCoord2f(1, 1)
        glVertex2f(x2, -1)
        glEnd()
        glFlush()
        glutPostRedisplay()
        
        glDeleteTextures([texID])
        
    # Load im1, im2 and phase contrasted image to the window
    imageTexture(cameraState.intermediate1, cameraState.ResX, cameraState.ResY, -1, -0.34, GL_FLOAT)
    imageTexture(cameraState.intermediate2, cameraState.ResX, cameraState.ResY, -0.31, 0.31, GL_FLOAT)
    imageTexture(cameraState.im, cameraState.ResX, cameraState.ResY, 0.34, 1, GL_FLOAT)
        
    # Copy the off-screen buffer to the screen.
    glutSwapBuffers()


# Function to loop if not paused
def Timer(value):
    if (cameraState.start == True):
        phaseContrastImage(cameraState)
    glutTimerFunc(value, Timer, value)


# Release camera and GPIO pins (at the end)
def cleanUp(camera):
    # Send a request to disconnect the python streamer
    socket.sio.emit('killPython')  
    #gpio.setmode(GPIO.BCM)
    gpio.output(14, gpio.LOW)
    gpio.output(18, gpio.LOW)
    gpio.cleanup()
    camera.close()
    

def main():
    cameraState.setUp(setup, gain)
    window = glutCreateWindow("Phase Contrast Microscopy")
    Timer(0)
    glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS)
    glutDisplayFunc(display)
    glutKeyboardFunc(keyPressed)
    glutMainLoop()
    print("Exited GLUT loop")
    print("Cleaning up")
    cleanUp(cameraState.camera)

if __name__ == "__main__":
    main()