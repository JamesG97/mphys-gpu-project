"""
State class file which handles the image arrays and the initial camera set up 
with the base images
"""

import numpy as np
from picamera import PiCamera
import RPi.GPIO as gpio
from time import sleep
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
from cameraSettings import *
from Gain import *

class State:
    # Need an instance of cameraSettings and Gain to be passed to the class 
    # for their functions
    def __init__(self, ResX, ResY):
                
        # Resolution of the images
        self.ResX = ResX
        self.ResY = ResY
        
        # Raw data of the base image to be subtracted
        self.im1BaseRaw = np.empty((self.ResY * self.ResX * 3) << 2, dtype = np.uint8)
        self.im2BaseRaw = np.empty((self.ResY * self.ResX * 3) << 2, dtype = np.uint8)
        
        # Y values of the images to be subtracted
        self.im1Base = np.empty((self.ResY * self.ResX), dtype = np.uint8)
        self.im2Base = np.empty((self.ResY * self.ResX), dtype = np.uint8)
        
        # Raw data of image 1 and 2
        self.im1Raw = np.empty((ResY * ResX * 3) << 2, dtype = np.uint8)
        self.im2Raw = np.empty((ResY * ResX * 3) << 2, dtype = np.uint8)
        
        # Y data only of image 1 and 2
        self.im1 = np.empty((ResY * ResX), dtype = np.uint8)
        self.im2 = np.empty((ResY * ResX), dtype = np.uint8)
        
        # Array for phase contrasted image, Y data only
        self.im = np.empty((ResY * ResX), dtype = np.float32)
        
        # Arrays to hold the image 1 and 2 data to be manipulated
        self.intermediate1 = np.empty((ResY, ResX), dtype = np.float32)
        self.intermediate2 = np.empty((ResY, ResX), dtype = np.float32)
        
        # Numerator and denominator in the phbase contrast intensity equation 
        self.Num = np.empty((ResY * ResX), dtype = np.float32)
        self.Denom = np.empty((ResY * ResX), dtype = np.float32)
        
        # Sleep time is for camera to adjust when LEDs turn on
        self.sleepTime = 0.01
        
        
    def setUp(self, camSet, gain):
        # Set up the camera
        self.camera = PiCamera()
        self.camera.resolution = (self.ResX, self.ResY)
        #self.camera.framerate = 24
        print("Camera set up")
        
        # Truth statement for the image loop in the main function
        self.start = False
                
        
        # Get the camera settings required from the cameraSettings class
        # The settings are saved to dictionaries in the class itself
        camSet.auto_expose_to_white(self.camera, 18)    # Image 1
        camSet.auto_expose_to_white(self.camera, 14)    # Image 2
        
        # Set the LED output pins
        gpio.setmode(gpio.BCM)
        gpio.setup(18, gpio.OUT)
        gpio.setup(14, gpio.OUT)
        
        # Make sure the LED output pins are both LOW
        gpio.output(18, gpio.LOW)
        gpio.output(14, gpio.LOW)
        
        # Get the sample free images for the base intensity removal
        print("Getting the sample-free base image data")
        
        # Setting camera settings for base image 1
        cameraSettings.switch_to_settings(self.camera, gain, camSet.camSettings1)
        gpio.output(18, gpio.HIGH)
        sleep(self.sleepTime)
        self.camera.capture(self.im1BaseRaw, 'yuv', use_video_port=True)
        gpio.output(18, gpio.LOW)

        # Setting camera settings for base image 2
        cameraSettings.switch_to_settings(self.camera, gain, camSet.camSettings2)
        gpio.output(14, gpio.HIGH)
        sleep(self.sleepTime)
        self.camera.capture(self.im2BaseRaw, 'yuv', use_video_port=True)
        gpio.output(14, gpio.LOW)
        
        # Take only the Y data from the raw base images
        self.im1Base = self.im1BaseRaw[:self.ResY * self.ResX]
        self.im2Base = self.im2BaseRaw[:self.ResY * self.ResX]
        
        print("Base image data obtained, insert sample. When inserted, press 'r' to start")
        print("\nCommands:\np: Pause \nr: Resume \ns: Save phase contrast image (right hand image) \nEsc: Exit program\n")
        
        # Initialise the OpenGL magic
        glutInit()
        glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)
        glutInitWindowSize(1500, 500)
        glutInitWindowPosition(100, 50)
    
    def printCameraSettings(self):
        print("Shutter speed: ")
        print(getattr(self.camera, 'shutter_speed'))
        print("Awb gains: ")
        print(getattr(self.camera, 'awb_gains'))
        print("Awb mode: ")
        print(getattr(self.camera, 'awb_mode'))
        print("Exposure mode: ")
        print(getattr(self.camera, 'exposure_mode'))
        print("Analog gain: ")
        print(getattr(self.camera, 'analog_gain'))
        print("Digital gain: ")
        print(getattr(self.camera, 'digital_gain'))
