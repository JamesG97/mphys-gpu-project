import socketio
import sys
import io
import os

import json
import base64

from PIL import Image
import time

class Socket:
    def __init__(self, ip='localhost', port='3000'):
        self.sio = socketio.Client()
               
        ## Callbacks from server events
        # Announce when connection established
        @self.sio.on('connect')
        def socket_connected():
            print("Connected as " + self.sio.eio.sid)

        @self.sio.on('disconnect')
        def socket_disconnected():
            print("Disconnected")

        # Disconnect on a kill event
        @self.sio.on("kill")
        def kill_connection():
            print("kill streamer connection")
            self.sio.disconnect()
           
        # Print message when one recieved
        @self.sio.on("new message")
        def message_received(message):
            print(message)
            
       
        # Connect the socket to the required location
        self.sio.connect('http://%s:%s' % (ip, port))

    def sendImage(self, image, width, height):
        data = {}
        data['time'] = str(int(1000 * time.time()))
        data['width'] = width
        data['height'] = height
        data['image'] = base64.b64encode(image).decode('utf-8')
        json_data = json.dumps(data)

        self.sio.emit("image", json_data)
