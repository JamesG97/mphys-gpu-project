#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 10:05:54 2019

@author: pi
"""
import RPi.GPIO as gpio
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
from PIL import Image
import numpy as np
import sys
import os
import itertools

#def loadTexture(Data, imgW, imgH):
#    # Generate a new texture from the image loaded
#    glEnable(GL_TEXTURE_2D)
#    texID = glGenTextures(1)
#    glBindTexture(GL_TEXTURE_2D, texID)
#    
#    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imgW, imgH, 0, GL_RGB, GL_UNSIGNED_BYTE, Data)
#
#    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
#    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
#    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
#    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
#
#    return texID
#
#def display():
#    # Clear the color and depth buffers
#    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
#
#    glBegin(GL_QUADS)
#    glTexCoord2f(0, 1)
#    glVertex2f(-0.75, -0.75)
#    glTexCoord2f(0, 0)
#    glVertex2f(-0.75, 0.75)
#    glTexCoord2f(1, 0)
#    glVertex2f(0.75, 0.75)
#    glTexCoord2f(1, 1)
#    glVertex2f(0.75, -0.75)
#    glEnd()
#
#    # Copy the off-screen buffer to the screen.
#    glutSwapBuffers()
#    
#def keyPressed(key, x, y):
#    # Exit program if the esc key is pressed
#    if (key == b'\x1b'):
#        exit(0)


cwd = os.getcwd()

gpio.setmode(gpio.BCM)
gpio.setup(18, gpio.OUT)
gpio.setup(14, gpio.OUT)

gpio.output(18, gpio.LOW)
gpio.output(18, gpio.LOW)
print("GPIOs set up")

# To be split into two images with spatially varied LEDs
gpio.output(18, gpio.HIGH)
os.system("raspistill -o " + cwd + "/Pictures/Image1.bmp")
gpio.output(18, gpio.LOW)

gpio.output(14, gpio.HIGH)
os.system("raspistill -o " + cwd + "/Pictures/Image2.bmp")
gpio.output(14, gpio.LOW)

print("Pictures taken")

im1 = Image.open(cwd + '/Pictures/Image1.bmp', 'r')
im2 = Image.open(cwd + '/Pictures/Image2.bmp', 'r')

#im.save('Picture.bmp')
#im = Image.save('Picture.bmp')

pix_val_1 = im1.getdata().convert('RGB')
#pix_val_ = im1.getdata()
#for i in range(10):
#    print(pix_val_[i][1])
#    print(pix_val_1[i][1])
#    print("\n")
pix_val_2 = im2.getdata().convert('RGB')

width1, height1 = im1.size
size1 = width1 * height1

width2, height2 = im2.size
size2 = width2 * height2

if size1 != size2:
    print("Images not the same size")
    sys.exit()
    
pix_val = [0] * size1

for i in range(size1):
        r1, g1, b1 = pix_val_1[i]
        r2, g2, b2 = pix_val_2[i]
        
        r_low = r1-r2
        g_low = g1-g2
        b_low = b1-b2
        
        r_high = r1+r2
        g_high = g1+g2
        b_high = b1+b2
        
        r = r_low/r_high
        g = g_low/g_high
        b = b_low/b_high
                
        rf = int(abs(r*255))
        gf = int(abs(g*255))
        bf = int(abs(b*255))
        
        pix_val[i] = (rf, gf, bf)
#        if i < 20:
#            print(r1, r_low, r_high)
#            #print("\n")
#            print(rf)
#            #print("\n")
#            print(pix_val_1[i])
#            print("\n\n")
            

#print(pix_val[0:50])

im2.putdata(pix_val)
im2.save(cwd + '/Pictures/Final.bmp')

data = [0] * len(pix_val) * 3
for i in range(len(pix_val)):
        r, g, b = pix_val[i]
        data[(i*3) + 0] = r
        data[(i*3) + 1] = g
        data[(i*3) + 2] = b


#data = list(itertools.chain(*pix_val)) 
#for i in range(10):
#    print(data[i])
#data = [x for y in pix_val for x in y]

gpio.cleanup()

#glutInit()
#glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)
#glutInitWindowSize(500, 500)
#glutInitWindowPosition(200, 100)
#
#wind = glutCreateWindow("The honorable gentleman's window")
#
#loadTexture(data, width1, height1)
#
#glutDisplayFunc(display)
#glutKeyboardFunc(keyPressed)
#glutMainLoop()