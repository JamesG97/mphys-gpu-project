#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 17 11:17:39 2019

@author: pi
"""

import RPi.GPIO as gpio

gpio.setmode(gpio.BCM)
gpio.setup(24, gpio.OUT)
gpio.setup(23, gpio.OUT)

text = ""

while text != "end":
    text = input("14; 18: Pin on. both: as stated on the box... none: ... end: leave. ")
    
    if text == "24":
        gpio.output(24, gpio.HIGH)
        gpio.output(23, gpio.LOW)
        
    elif text == "23":
        gpio.output(23, gpio.HIGH)
        gpio.output(24, gpio.LOW)
        
    elif text == "both":
        gpio.output(24, gpio.HIGH)
        gpio.output(23, gpio.HIGH)
        
    elif text == "none":
        gpio.output(23, gpio.LOW)
        gpio.output(24, gpio.LOW)
        
    else:
        break

gpio.output(24, gpio.LOW)
gpio.output(23, gpio.LOW)
gpio.cleanup()