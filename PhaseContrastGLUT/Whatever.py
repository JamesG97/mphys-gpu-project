from picamera import PiCamera
import RPi.GPIO as gpio

gpio.setmode(gpio.BCM)
gpio.setup(18, gpio.OUT)
gpio.output(18, gpio.HIGH)

camera = PiCamera()
camera.start_preview(fullscreen=False, window=(0,50,640,480))

text = ""

while text != "end":
    text = input("end: leave. ")



camera.stop_preview()
gpio.output(18, gpio.LOW)
gpio.cleanup()