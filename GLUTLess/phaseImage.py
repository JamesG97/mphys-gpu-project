"""
Main phase contrast file, with the phase contrast image function, the assorted 
OpenGL window and loop functions and the main function which runs it all
"""

import RPi.GPIO as gpio
import numpy as np
from time import sleep
from PIL import Image
from datetime import datetime
from State import *
from cameraSettings import *
from Gain import *
from Socket import * 
import atexit

# Set up the 2 dependent classes which are required in the cameraState functions
setup = cameraSettings()
gain = Gain()
# Create an instance of the camera's State class
cameraState = State(640, 480)

# Create an instance of the Socket class (connected to localhost:3000)
socket = Socket(ip='127.0.0.1', port='3000')

# Function to take two images, go through phase contrast maths and get the phase contrasted image
def phaseContrastImage(state):
    def toggleSettings(i):
        cameraSettings.switch_to_settings(state.camera, gain, setup.camSettings1 if (i % 2 == 0) else setup.camSettings2)
    
    tStart = datetime.now()
    c = 0
    for i in enumerate(state.camera.capture_continuous(state.im1, format='jpeg', quality=10, use_video_port=True)):
        socket.sendImage(state.im1.getvalue())
        
        state.im1.truncate()
        state.im1.seek(0)
        
        c += 1
        toggleSettings(c)
        
        if (c % 100 == 0):
            print("{:.3f}  (fps)".format(100.0 / ((datetime.now() - tStart).total_seconds())))
            tStart = datetime.now()
            
     

# Release camera and GPIO pins (at the end)
def cleanUp(camera):
    print("Clean up")
    
    # Send a request to disconnect the python streamer
    socket.sendDisconnectionRequest()
    
    # Close the instance of the camera
    camera.close()
    
    # Set all gpio pins low
    #gpio.setmode(GPIO.BCM)
    #gpio.output(14, gpio.LOW)
    #gpio.output(18, gpio.LOW)
    #gpio.cleanup()
    
    

def main():
    socket.sendMessage("Setting up camera, please do not insert sample")
    
    cameraState.setUp(setup, gain, fixExposure=True)
    atexit.register(cleanUp, cameraState.camera)
    
    socket.setBaseImages(cameraState.im1Base.getvalue(), cameraState.im2Base.getvalue())
    socket.sendMessage("Setup complete")
    
    phaseContrastImage(cameraState)
    

if __name__ == "__main__":
    main()