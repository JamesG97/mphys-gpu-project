import socketio
import sys
import io
import os

import json
import base64

from PIL import Image
import time

class Socket:
    def __init__(self, ip='localhost', port='3000'):
        self.sio = socketio.Client()
               
        ## Callbacks from server events
        # Announce when connection established
        @self.sio.on('connect')
        def socket_connected():
            print("Connected as " + self.sio.eio.sid)

        @self.sio.on('disconnect')
        def socket_disconnected():
            print("Disconnected")

        # Disconnect on a kill event
        @self.sio.on("kill")
        def kill_connection():
            print("kill streamer connection")
            self.sio.disconnect()
        
        @self.sio.on("base_images_requested")
        def base_images_requested():
            self.sio.emit("bImage0", self.baseIm0)
            self.sio.emit("bImage1", self.baseIm1)
       
        # Connect the socket to the required location
        self.sio.connect('http://%s:%s' % (ip, port))

    def setBaseImages(self, baseIm0, baseIm1):
        self.baseIm0 = baseIm0
        self.baseIm1 = baseIm1
        self.sio.emit("bImage0", self.baseIm0)
        self.sio.emit("bImage1", self.baseIm1)

    def sendDisconnectionRequest(self):
        self.sio.emit('killPython')
        
    def sendImage(self, image):
        self.sio.emit("image", image)
        
    def sendMessage(self, msg):
        self.sio.emit("message", msg)
