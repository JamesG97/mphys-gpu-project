#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 17 11:17:39 2019

@author: pi
"""

import RPi.GPIO as gpio

gpio.setmode(gpio.BCM)
gpio.setup(14, gpio.OUT)
gpio.setup(18, gpio.OUT)

text = ""

while text != "end":
    text = input("14; 18: Pin on. both: as stated on the box... none: ... end: leave. ")
    
    if text == "14":
        gpio.output(14, gpio.HIGH)
        gpio.output(18, gpio.LOW)
        
    elif text == "18":
        gpio.output(18, gpio.HIGH)
        gpio.output(14, gpio.LOW)
        
    elif text == "both":
        gpio.output(14, gpio.HIGH)
        gpio.output(18, gpio.HIGH)
        
    elif text == "none":
        gpio.output(18, gpio.LOW)
        gpio.output(14, gpio.LOW)
        
    else:
        break

gpio.output(14, gpio.LOW)
gpio.output(18, gpio.LOW)
gpio.cleanup()