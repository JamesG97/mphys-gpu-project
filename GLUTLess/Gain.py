"""
Gain class file which handles setting the analogue and digital gain as these 
cannot simply be set with setattr. Functions handle setting MMAL camera 
parameters instead.
"""

from picamera import mmal, exc
from picamera.mmalobj import to_rational

class Gain:
    def __init__(self):
        self.MMAL_PARAMETER_ANALOG_GAIN = mmal.MMAL_PARAMETER_GROUP_CAMERA + 0x59
        self.MMAL_PARAMETER_DIGITAL_GAIN = mmal.MMAL_PARAMETER_GROUP_CAMERA + 0x5A
    
    def set_gain(self, camera, gain, value):
        """
        Set the analog gain of a PiCamera.
        
        camera: the picamera.PiCamera() instance you are configuring
        gain: either MMAL_PARAMETER_ANALOG_GAIN or MMAL_PARAMETER_DIGITAL_GAIN
        value: a numeric value that can be converted to a rational number.
        """
        if gain not in [self.MMAL_PARAMETER_ANALOG_GAIN, self.MMAL_PARAMETER_DIGITAL_GAIN]:
            raise ValueError("The gain parameter was not valid")
        ret = mmal.mmal_port_parameter_set_rational(camera._camera.control._port, 
                                                        gain,
                                                        to_rational(value))
        if ret == 4:
            raise exc.PiCameraMMALError(ret, "Are you running the latest version of the userland libraries? Gain setting was introduced in late 2017.")
        elif ret != 0:
            raise exc.PiCameraMMALError(ret)

    def set_analog_gain(self, camera, value):
        """Set the gain of a PiCamera object to a given value."""
        self.set_gain(camera, self.MMAL_PARAMETER_ANALOG_GAIN, value)
    
    def set_digital_gain(self, camera, value):
        """Set the digital gain of a PiCamera object to a given value."""
        self.set_gain(camera, self.MMAL_PARAMETER_DIGITAL_GAIN, value)