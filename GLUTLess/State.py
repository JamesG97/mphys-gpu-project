"""
State class file which handles the image arrays and the initial camera set up 
with the base images
"""

import numpy as np
from picamera import PiCamera
import RPi.GPIO as gpio
from time import sleep
from cameraSettings import *
from Gain import *
import io

class State:
    # Need an instance of cameraSettings and Gain to be passed to the class 
    # for their functions
    def __init__(self, ResX, ResY):
                
        # Resolution of the images
        self.ResX = ResX
        self.ResY = ResY
        
        # JPEG encoded byte stream for the camera feed
        self.im1 = io.BytesIO()
        
        # JPEG encoded byte arrays for the base images, taken in each of the LED configurations without any samples present
        self.im1Base = io.BytesIO()
        self.im2Base = io.BytesIO()
        
        # Sleep time is for camera to adjust when LEDs turn on
        self.sleepTime = 0.01
        
        
    def setUp(self, camSet, gain, fixExposure=True):  
        # Set up the camera
        self.camera = PiCamera()
        self.camera.resolution = (self.ResX, self.ResY)
        #self.camera.framerate = 24
        print("Camera set up")
        
        # Truth statement for the image loop in the main function
        self.isRunning = False
        
        if (fixExposure):
            # Get the camera settings required from the cameraSettings class
            # The settings are saved to dictionaries in the class itself
            camSet.auto_expose_to_white(self.camera, 18)    # Image 1
            camSet.auto_expose_to_white(self.camera, 14)    # Image 2
            
            
            # Set the LED output pins
            gpio.setmode(gpio.BCM)
            gpio.setup(18, gpio.OUT)
            gpio.setup(14, gpio.OUT)
            
            # Make sure the LED output pins are both LOW
            gpio.output(18, gpio.LOW)
            gpio.output(14, gpio.LOW)
            
            
            # Get the sample free images for the base intensity removal
            print("Getting the sample-free base image data")
            
            # Setting camera settings for base image 1
            cameraSettings.switch_to_settings(self.camera, gain, camSet.camSettings1)
            gpio.output(18, gpio.HIGH)
            sleep(self.sleepTime)
            self.camera.capture(self.im1Base, 'jpeg', use_video_port=True)
            gpio.output(18, gpio.LOW)

            # Setting camera settings for base image 2
            cameraSettings.switch_to_settings(self.camera, gain, camSet.camSettings2)
            gpio.output(14, gpio.HIGH)
            sleep(self.sleepTime)
            self.camera.capture(self.im2Base, 'jpeg', use_video_port=True)
            gpio.output(14, gpio.LOW)
            
            print("Base image data obtained, insert sample")
            
    
    def printCameraSettings(self):
        print("Shutter speed: ")
        print(getattr(self.camera, 'shutter_speed'))
        print("Awb gains: ")
        print(getattr(self.camera, 'awb_gains'))
        print("Awb mode: ")
        print(getattr(self.camera, 'awb_mode'))
        print("Exposure mode: ")
        print(getattr(self.camera, 'exposure_mode'))
        print("Analog gain: ")
        print(getattr(self.camera, 'analog_gain'))
        print("Digital gain: ")
        print(getattr(self.camera, 'digital_gain'))
