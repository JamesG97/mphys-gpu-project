"""
cameraSettings class which handles obtaining and saving the camera settings for 
each LED. Can also save and restore the settings from a file.
"""

from picamera.array import PiRGBArray
import RPi.GPIO as gpio
import numpy as np
from time import sleep
#import yaml

class cameraSettings:
    def __init__(self):
        # Dictionaries to hold the camera settings for image 1 and 2
        self.camSettings1 = {}
        self.camSettings2 = {}
        
    
    def rgb_image(self, camera, resize=None, **kwargs):
        """Capture an image and return an RGB numpy array"""
        with PiRGBArray(camera, size=resize) as output:
            camera.capture(output, format='rgb', resize=resize, **kwargs)
            return output.array

    # Set the camera for images
    @staticmethod
    def switch_to_settings(camera, gain, cameraSettings):
        camera.shutter_speed = cameraSettings.get('shutter_speed')
        camera.awb_mode = cameraSettings.get('awb_mode')
        camera.awb_gains = cameraSettings.get('awb_gains')
        camera.exposure_mode = cameraSettings.get('exposure_mode')
        gain.set_analog_gain(camera, cameraSettings.get('analog_gain'))
        gain.set_digital_gain(camera, cameraSettings.get('digital_gain'))
    
    
    def auto_expose_to_white(self, camera, led):
        """Freeze the settings after auto-exposing to white illumination"""
        print("Turning on the LED and letting the camera auto-expose", end="")
        
        gpio.setmode(gpio.BCM)
        gpio.setup(int(led), gpio.OUT)
        gpio.output(int(led), gpio.HIGH)
        
        #camera.start_preview(fullscreen=False, window=(0,50,640,480))
        for i in range(0):
            print(".", end="")
            sleep(0.5)
        print("done")

        print("Freezing the camera settings...")
        camera.shutter_speed = camera.exposure_speed
        #print("Shutter speed = {}".format(camera.shutter_speed))
        camera.exposure_mode = "off"
        #print("Auto exposure disabled")
        g = camera.awb_gains
        camera.awb_mode = "off"
        camera.awb_gains = g
        #print("Auto white balance disabled, gains are {}".format(g))
        #print("Analogue gain: {}, Digital gain: {}".format(camera.analog_gain, camera.digital_gain))

        print("Adjusting shutter speed to avoid saturation", end="")
        for i in range(3):
            print(".", end="")
            camera.shutter_speed = int(camera.shutter_speed * 640.0 / np.max(self.rgb_image(camera)))
            sleep(1)
        print("done")
        
        #print("Cleaning up GPIO")
        gpio.output(int(led), gpio.LOW)
        camera.stop_preview()
        
        if (int(led) == 18):
            self.camSettings1 = {k: getattr(camera, k) for k in ['analog_gain', 'digital_gain', 'shutter_speed', 'awb_gains', 'awb_mode', 'exposure_mode']}
#            print("\ncamSettings1:")
#            for k, v in self.camSettings1.items():
#                print(k, v)
        elif (int(led) == 14):
            self.camSettings2 = {k: getattr(camera, k) for k in ['analog_gain', 'digital_gain', 'shutter_speed', 'awb_gains', 'awb_mode', 'exposure_mode']}
#            print("\n camSettings2:")
#            for k, v in self.camSettings2.items():
#                print(k, v)
#        gpio.cleanup()
        
#    def save_settings(self, camera, output="output/camera_settings.yaml"):
#        """Save the camera settings to a YAML file"""
#        camera_settings = {k: getattr(camera, k) for k in ['analog_gain', 'digital_gain', 'shutter_speed', 'awb_gains', 'awb_mode', 'exposure_mode']}
#        with open(output, "w") as outfile:
#            yaml.dump(camera_settings, outfile)
#    
#    # Currently broken as you can't set the analog and digital gain attributes like this
#    def restore_settings(self, camera, filename, ignore=['analog_gain', 'digital_gain']):
 ##       """Load camera settings from a YAML file"""
  ##      with open(filename, "r") as infile:
  #          settings = yaml.load(infile, Loader=yaml.FullLoader)
   ##         for k, v in settings.items():
    #            if k not in ignore:
    ##                setattr(camera, k, v)