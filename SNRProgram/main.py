# -*- coding: utf-8 -*-
"""
Created on Sun Dec  8 18:15:10 2019

@author: Jack
"""

import numpy as np
from numpy import errstate, isneginf
import matplotlib.pyplot as plt
import os
import cv2

from shapes import Line, Rect
from mouse_data import MouseData

# Global Variables
res_x, res_y = 1280, 720

image_index = 0         # Index of the image in the list of all images
current_image = None    # The current image being processed
shown_image = None      # The image to show including GUI

lines = []              # The list of all lines to draw
rects = []              # The list of all rectangles to draw

crop_rect = None

scale_factor = 1        # pixels in the image / Pixels in the window


directory = "../All_Data/Final/EmptyStationary_1/processed_stack"

image_count = 0
all_image_names = []
angle = 0

# GUI Utilities
# =============================================================================  

def is_valid_file(name):
    global directory
    return  ".jpg" in name or ".jpeg" in name or \
            ".png" in name or ".bmp" in name or \
            ".tif" in name or ".tiff" in name or \
            ".gif" in name

def sort_images():
    global directory, all_image_names, image_count
        
    def get_number(name):
        return int(name.split("_")[1].split(".")[0])
    
    for f in os.listdir(directory):
        if is_valid_file(f):
            all_image_names.append(f) 
            image_count += 1 
    
    try:        
        all_image_names = sorted(all_image_names, key=get_number)
    except:
        return          

# Images allows iterating over all images in the file   
def image(index):  
    global directory, all_image_names, image_count
    
    return np.asarray(cv2.imread("/".join([directory, all_image_names[index % image_count]])))

def set_current_image(index):
    global current_image, scale_factor, crop_rect, angle
    
    # Show the current image
    current_image = image(index)
    
    # Get the scale factor
    scale_factor = int(np.ceil(current_image.shape[1] / res_x) / 1.5)
    if scale_factor < 1: scale_factor = 1
    
    if angle != 0:
        rotate_image()
        
    if crop_rect != None:
        current_image = intensity_within_all_channels(crop_rect)
        

def crop_to_rect(rect):
    global current_image, crop_rect
    
    if rect.dx > 10 and rect.dy > 10:
        rects.clear()
        
        crop_rect = rect
        current_image = intensity_within_all_channels(crop_rect)
        
def default_zoom():
    global current_image, image_index, crop_rect
    crop_rect = None
    current_image = image(image_index)
    
    rotate_image()
    
def rotate_image():
    global current_image, angle
    
    image_center = tuple(np.array(current_image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    current_image = cv2.warpAffine(current_image, rot_mat, current_image.shape[1::-1], flags=cv2.INTER_LINEAR)

# Draws a line on the current image
def draw_line(line):
    global shown_image, scale_factor

    for r in range(int(line.dr)):
        y, x = line.pos(r)
        
        # If in range draw the part of the line
        if x > scale_factor + 3 and x < (shown_image.shape[1] - scale_factor - 3) and \
           y > scale_factor + 3 and y < (shown_image.shape[0] - scale_factor - 3):
               B = np.multiply(line.brightness, np.ones(((2*scale_factor)+1, (2*scale_factor)+1, 3)))
               shown_image[int(int(y)-scale_factor): int(int(y)+scale_factor+1), int(int(x)-scale_factor): int(int(x)+scale_factor+1)] = B
               
# Draws a line on the current image
def draw_rect(rect):
    global shown_image, scale_factor

    dr_total = 2 * (rect.dx + rect.dy)
    
    for r in range(dr_total):
        y, x = rect.pos(r / dr_total)
            
        # If in range draw the part of the line
        if x > scale_factor + 3 and x < (shown_image.shape[1] - scale_factor - 3) and \
           y > scale_factor + 3 and y < (shown_image.shape[0] - scale_factor - 3):
               B = np.multiply(rect.brightness, np.ones(((2*scale_factor)+1, (2*scale_factor)+1, 3)))
               shown_image[int(int(y)-scale_factor): int(int(y)+scale_factor+1), int(int(x)-scale_factor): int(int(x)+scale_factor+1)] = B

 
    
# Line Operations
# =============================================================================
                    
# Get all pixel values of the current image along a given line
def intensity_along(line): 
    global current_image
    
    R = []
    I = []
    for r in range(int(line.dr)):
        R.append(r)
        pos = line.ipos(r)
        I.append(current_image[pos[0], pos[1], 1])
        
    # Return the intensity along the path
    if R != None:
        return R, I
    
    return [0], [0]
   
# Returns the subset of X and Y where Filter[i] == True
def filter_data(X, Y, Filter):
    if len(X) != len(Y) or len(Y) != len(Filter) or len(Filter) < 3: return
    
    num = sum(Filter)
    x_new = np.zeros(num)
    y_new = np.zeros(num)
    
    n = 0
    for i in range(len(Filter)):
        if Filter[i]:
            x_new[n] = X[i]
            y_new[n] = Y[i]
            n += 1
    
    return x_new, y_new

# Get the maxima within the given data
def get_maxima(I):
    if len(I) < 3: return
    maxima = np.zeros(len(I), dtype=bool)
    
    # First and last values have 1 comparison
    maxima[0] = I[0] >= I[1];  maxima[-1] = I[-1] >= I[-2]
    
    # Inner values compared to either side
    maxima[1:-1] = np.multiply(np.less_equal(I[2:], I[1:-1]),\
                               np.less_equal(I[:-2], I[1:-1]))
    
    return maxima

# Get the maxima within the given data
def get_minima(I):
    if len(I) < 3: return
    minima = np.zeros(len(I), dtype=bool)
    
    # First and last values have 1 comparison
    minima[0] = I[0] <= I[1];  minima[-1] = I[-1] <= I[-2]
    
    # Inner values compared to either side
    minima[1:-1] = np.multiply(np.greater_equal(I[2:], I[1:-1]),\
                               np.greater_equal(I[:-2], I[1:-1]))
    
    return minima



# Rect Operations
# =============================================================================

def whole_screen():
    global current_image
    
    return Rect((0, 0), (current_image.shape[1], current_image.shape[0]))

# Get the intensity within the rectangle
def intensity_within(rect):
    global current_image
    
    return current_image[rect.y0i:rect.y1i, rect.x0i:rect.x1i, 1]

# Get the intensity within the rectangle
def intensity_within_all_channels(rect):
    global current_image
    
    return current_image[rect.y0i:rect.y1i, rect.x0i:rect.x1i, :]

def mesh_grid_rect(rect):
    x = np.arange(rect.x0i, rect.x1i)
    y = np.arange(rect.y0i, rect.y1i)
    return np.meshgrid(x, y)

def mesh_grid_rect_fourier(rect):
    # Number of samples in each direction
    Nx = rect.dx
    Ny = rect.dy
    
    # 1 sample per pixel -> 0.5 is maximum frequency
    x_f = np.linspace(-0.5, 0.5, Nx)
    y_f = np.linspace(-0.5, 0.5, Ny)
    
    return np.meshgrid(x_f, y_f)
    
def fourier_transform(rect, take_log=True):
    I = intensity_within(rect)
    I_f = np.fft.fftshift(np.fft.fft2(I))
    
    if take_log:
        return log_safe(I_f)
    else:
        return I_f

def log_safe(data):
    with errstate(divide='ignore'):
        data = np.log(data)
        data[isneginf(np.real(data))]=0
        return data


# Whole Stack Operations
# =============================================================================

def average_image(rect):
    global image_index, image_count
    
    print("Averaging:")
    steps = 50
    print("_" * steps)
    total = image_count
    progress_step_size = int(total / steps)
    
    I_avg = np.zeros((rect.dy, rect.dx))            
    for i in range(total):
        set_current_image(i)
        I_avg = np.add(I_avg, intensity_within(rect))
        
        if progress_step_size > 0 and i % progress_step_size == 0 and i > 0:
            print("*", end='')
    print("\n")
    
    set_current_image(image_index)        
    return np.divide(I_avg, image_count)
    
    
def std_dev_image(rect):
    global image_index, image_count
    
    print("Getting std deviation:")
    steps = 50
    print("_" * steps)
    total = image_count
    progress_step_size = int(total / steps)
    print(total)
    I_all = np.zeros((rect.dy, rect.dx, image_count))           
    for i in range(image_count):
        set_current_image(i)
        I_all[:, :, i] = intensity_within(rect)
        
        if progress_step_size > 0 and i % progress_step_size == 0 and i > 0:
            print("*", end='')
    print("\n")
    
    I_std = np.std(I_all, axis=2, ddof=1)
    
    set_current_image(image_index)        
    return I_std
    
def std_dev_image_normalised(rect):
    global image_index, image_count
    
    print("Getting normalised std deviation:")
    I_avg = average_image(rect)
    
    print("Getting std deviation:")
    steps = 50
    print("_" * steps)
    total = image_count
    progress_step_size = int(total / steps)
    
    I_all = np.zeros((rect.dy, rect.dx, image_count))           
    for i in range(image_count):
        set_current_image(i)
        I_all[:, :, i] = np.subtract(intensity_within(rect), I_avg)
        
        if progress_step_size > 0 and i % progress_step_size == 0 and i > 0:
            print("*", end='')
    print("\n")
    
    I_std = np.std(I_all, axis=2, ddof=1)
    
    set_current_image(image_index)        
    return I_std

def fixed_pattern_noise(rect):
    
    I_avg = average_image(rect)
    I_std = np.std(I_avg, ddof=1)
    
    print("Fixed pattern noise: {}".format(I_std))

def random_noise(rect):
    I_std = std_dev_image(rect)
    I_avg = np.mean(I_std)
    
    print("Random noise: {}".format(I_avg))

# Outputs
# =============================================================================
    
# Plots intensity data showing extrema if desired
def plot_data(R, I, show_maxima=True, show_minima=True):     
    # Plot intensity along path
    plt.plot(R, I, 'kx-')
    
    # Plot extreme values if desired and possible
    if len(I) >= 3:
        if show_maxima:
            maxima = get_maxima(I) 
            x, y = filter_data(R, I, maxima)
            plt.plot(x, y, 'rx')
        
        if show_minima:
            minima = get_minima(I)
            x, y = filter_data(R, I, minima)
            plt.plot(x, y, 'bx')
        
    # Show the plot
    plt.show()
    print("_" * 80)

def contour_data(XX, YY, II, n=50, v_min=np.finfo(float).min, v_max=np.finfo(float).max):
    
    if v_min == np.finfo(float).min:
        v_min = np.min(II)
    if v_max == np.finfo(float).max:
        v_max = np.max(II)
        
    v = np.linspace(v_min, v_max, n, endpoint=True)
    v_reduced= np.linspace(v_min, v_max, 10, endpoint=True)
    
    II = np.clip(II[:,:], v_min, v_max)
    contour = plt.contourf(XX, YY, II, v)
    plt.axis('equal')
    plt.colorbar(contour, ticks=v_reduced)
    plt.show()
    print("_" * 80)
 
    
# -----------------------------------------------------------------------------




# Mouse + Keyboard Events
# =============================================================================

md_left = MouseData()
md_right = MouseData()
def mouse_input(event, x, y, flags, param):
    global lines, angle, image_index
      
    # If mouse moved, update line 
    if event == 0:
        md_left.mouse_pos = (x, y)
        md_right.mouse_pos = (x, y)
        
        if md_left.is_mouse_down:
            lines[-1].end = md_left.mouse_pos
            
        if md_right.is_mouse_down:
            rects[-1].end = md_right.mouse_pos
            
    # If left mouse is pressed
    if event == 1:        
        md_left.is_mouse_down = True
        md_left.mouse_down_pos = (x, y)
        
        lines.clear()
        
        l = Line(md_left.mouse_down_pos, md_left.mouse_pos, channel=2)
        lines.append(l)
    
    # If right mouse is pressed
    if event == 2:
        md_right.is_mouse_down = True
        md_right.mouse_down_pos = (x, y)
        
        rects.clear()
        
        r = Rect(md_right.mouse_down_pos, md_right.mouse_pos, channel=2)
        rects.append(r)
        
    # If left mouse up
    if event == 4:
        md_left.is_mouse_down = False
        md_left.is_mouse_up = True
        md_left.mouse_up_pos = (x, y)
        
        l = Line(md_left.mouse_down_pos, md_left.mouse_up_pos)
        rects.clear()
        lines.clear()
        if l.dr > 0:
            lines.append(l)
    
    # If right mouse up
    if event == 5:
        md_right.is_mouse_down = False
        md_right.is_mouse_up = True
        md_right.mouse_up_pos = (x, y)
        
        r = Rect(md_right.mouse_down_pos, md_right.mouse_up_pos)
        
        lines.clear()
        rects.clear()
        if r.dr > 0:
            rects.append(r)
    
    # Scroll to rotate
    if event == 10:
        angle += 2.5 * np.sign(flags)
        set_current_image(image_index)
        
    # Scroll button to reset rotation
    if event == 3:
        angle = 0
        set_current_image(image_index)
          
          
def keyboard_input(key, x, y):
    global image_index, directory, image_count, all_image_names
    
    # Plot data (p)
    if key == ord('p'):
        if len(lines) > 0:
            R, I = intensity_along(lines[-1])
            plot_data(R, I)
        elif len(rects) > 0:
            I = intensity_within(rects[-1])
            XX, YY = mesh_grid_rect(rects[-1])
            contour_data(XX, YY, I)
        return 0
    
    # Crop (c)
    if key == ord('c'):
        if len(rects) > 0:
            crop_to_rect(rects[-1])
        else:
            default_zoom()
       
    # Average of each pixel in stack (a)
    if key == ord('a'):
        # If there is a rectangle, plot the average of all intensities through the stack
        if len(rects) > 0:
            rect = rects[-1]
        else:
            rect = whole_screen()
            
        I_avg = average_image(rect)
        XX, YY = mesh_grid_rect(rect)
        contour_data(XX, YY, I_avg)
            
    # Std Dev of pixels in stack (s)
    if key == ord('s'):
        # If there is a rectangle, plot the std dev of all intensities through the stack
        if len(rects) > 0:
            rect = rects[-1]
        else:
            rect = whole_screen()
            
        I_std = std_dev_image(rect)
        XX, YY = mesh_grid_rect(rect)
        contour_data(XX, YY, I_std) # Std Dev of pixels in stack (s)
    
    # Normalised std dev of each pixel in stack (d)
    if key == ord('d'):
        # If there is a rectangle, plot the std dev of all intensities through the stack
        if len(rects) > 0:
            rect = rects[-1]
        else:
            rect = whole_screen()
            
        I_std = std_dev_image_normalised(rect)
        XX, YY = mesh_grid_rect(rect)
        contour_data(XX, YY, I_std) 
       
    # Fourier (t)
    if key == ord('t'):
        print("Fourier Transform")
        if len(rects) > 0:
            rect = rects[-1]
        else:
            rect = whole_screen()
            
        I = fourier_transform(rect)
        XX, YY = mesh_grid_rect_fourier(rect)
        contour_data(XX, YY, np.real(I), v_min=0) 

                
    # Random noise (r)
    if key == ord('r'):
        if len(rects) > 0:
            rect = rects[-1]
        else:
            rect = whole_screen()
            
        print("Random noise")
        random_noise(rect)
    # Average of each pixel in stack (f)
    if key == ord('f'):
        if len(rects) > 0:
            rect = rects[-1]
        else:
            rect = whole_screen()
            
        print("Fixed pattern noise:")
        fixed_pattern_noise(rect)
        
        
    # Change directory
    if key == ord('n'):
        current_directory = directory.split('/')
        if current_directory[-1] == "processed_stack":
            current_directory[-1] = "brightfield_stack"
        else:
            current_directory[-1] = "processed_stack"
        
        directory = ""
        for i in range(len(current_directory)):
            directory += current_directory[i]
            if i < len(current_directory) - 1:
                directory += "/"
                
        image_count = 0
        all_image_names = []
        sort_images()
        set_current_image(image_index)
        draw()
        
        
    # Exit (Esc)
    if key == 27:
        return -1
    
    # Next (Space)
    elif key == 32:
        image_index += 1
        image_index %= image_count
        return 1
    
    # Prev (Backspace) 
    if key == 8:
        image_index -= 1
        image_index %= image_count
        return 1
    
    # Anything else (inc. nothing)
    else:    
        #if key != -1:
        #    print(key)
        return 0
     
# -----------------------------------------------------------------------------
    
    

# Main
# =============================================================================        
    
def main():   
    global shown_image, current_image, lines, image_index, scale_factor, res_x, res_y, angle
    
    sort_images()
    res_x, res_y = 1280, 720
    
    cv2.namedWindow("image", cv2.WINDOW_NORMAL)
    cv2.resizeWindow("image", res_x, res_y)
    cv2.setMouseCallback("image", mouse_input)
    

    # Iterate over all images in the images folder
    while True:
        set_current_image(image_index)
        
        # Listen for user inputs on keyboard
        end = False
        while True:
            
            ## MOUSE HANDLED BY CV2 CALLBACK
            
            ## KEYBOARD
            key = cv2.waitKey(20)
            k = keyboard_input(key, md_left.mouse_pos[0], md_left.mouse_pos[1])
            if k == -1:
                end = True
                break
            elif k == 1:
                break
            
            ## Draw image
            draw()
            
        if end:
            break
        
    cv2.destroyAllWindows()
    
# Draw the image and GUI and show it
def draw():
    global shown_image, current_image, image_index
    
    # Reset image to current image
    shown_image = current_image.copy()
    
    # Draw lines
    for line in lines:
        draw_line(line)
        
    # Draw rects
    for rect in rects:
        draw_rect(rect)
        
    font                   = cv2.FONT_HERSHEY_SIMPLEX
    bottomLeftCornerOfText = (10,50)
    fontScale              = 1
    fontColor              = (255,255,255)
    lineType               = 2
    
    cv2.putText(shown_image, 
        "Image {} {}".format(image_index, 'bf' if directory.split('/')[-1] == "brightfield_stack" else 'pc'), 
        bottomLeftCornerOfText, 
        font, 
        fontScale,
        fontColor,
        lineType)
    
    # Show the result
    cv2.imshow("image", shown_image)
    
    
# Main caller
if __name__ == '__main__':
    main()
