# -*- coding: utf-8 -*-
"""
Created on Tue Dec 10 22:53:03 2019

@author: Jack
"""

# Mouse data stores mouse position at key events
class MouseData:    
    def __init__(self):
        self.is_mouse_down = False 
        self._is_mouse_up = False
        self._mouse_down_x, self._mouse_down_y = -1, -1
        self._mouse_up_x, self._mouse_up_y = -1, -1
        self._mouse_pos_x, self._mouse_pos_y = -1, -1
        self._mouse_prev_pos_x, self._mouse_prev_pos_y = -1, -1
    
    # 4 Properties for mouse positions; now, prev, when last clicked down, and when last lifted up
    
    def __set_mouse_pos(self, pos):
        self._mouse_prev_pos_x, self._mouse_prev_pos_y = self._mouse_pos_x, self._mouse_pos_y
        self._mouse_pos_x, self._mouse_pos_y = pos    
    mouse_pos = property(lambda self: (self._mouse_pos_x, self._mouse_pos_y), __set_mouse_pos)
     
    mouse_prev_pos = property(lambda self: (self._mouse_prev_pos_x, self._mouse_prev_pos_y))
    
    def __set_mouse_down_pos(self, pos):
        self._mouse_down_x, self._mouse_down_y = pos
    mouse_down_pos = property(lambda self: (self._mouse_down_x, self._mouse_down_y), __set_mouse_down_pos)
    
    def __set_mouse_up_pos(self, pos):
        self._mouse_up_x, self._mouse_up_y = pos
    mouse_up_pos = property(lambda self: (self._mouse_up_x, self._mouse_up_y), __set_mouse_up_pos)
    
    
    # Is mouse up will return true for the first time it is queried 
    # after the mouse up event
    def _set_is_mouse_up(self, value):
        self._is_mouse_up = value
        
    def _get_is_mouse_up(self): 
        was_mouse_up = self._is_mouse_up; self._is_mouse_up = False
        return was_mouse_up    
    is_mouse_up = property(_get_is_mouse_up, _set_is_mouse_up)
     