# -*- coding: utf-8 -*-
"""
Created on Tue Dec 10 22:47:40 2019

@author: Jack
"""

import numpy as np

# LINES
# =============================================================================

# Easier to draw line over image than modifying the data
class Line:
    def __init__(self, start, end, channel = 1, brightness = 255):
        # Store data about the line
        self.start = start
        self.end = end
        self.channel = channel
        self.brightness = brightness
        
    
    # Lines can be changed so reassign when necessary
    x0 = property(lambda self: self.start[0])
    y0 = property(lambda self: self.start[1])
    x1 = property(lambda self: self.end[0])
    y1 = property(lambda self: self.end[1])
    
    dx = property(lambda self: self.x1 - self.x0)
    dy = property(lambda self: self.y1 - self.y0)
    dr = property(lambda self: np.sqrt((self.dx**2) + (self.dy**2)))
    
    
    # Position given in form (y, x) because opencv is weird 
    # for r from 0 to dr
    def pos(self, r):
        return self.y0 + (self.dy * r / self.dr), self.x0 + (self.dx * r / self.dr)
    
    def ipos(self, r):
        p = self.pos(r)
        return int(p[0]), int(p[1])

# -----------------------------------------------------------------------------


# RECTANGLES
# =============================================================================

# Easier to draw line over image than modifying the data
class Rect(Line):
    def __init__(self, start, end, channel = 1, brightness = 255):
        super().__init__(start, end, channel = 1, brightness = 255)
        
    
    # Lines can be changed so reassign when necessary
    x0 = property(lambda self: min(self.start[0], self.end[0]))
    x0i = property(lambda self: int(self.x0))
    y0 = property(lambda self: min(self.start[1], self.end[1]))
    y0i = property(lambda self: int(self.y0))
    
    x1 = property(lambda self: max(self.start[0], self.end[0]))
    x1i = property(lambda self: int(self.x1))
    y1 = property(lambda self: max(self.start[1], self.end[1]))
    y1i = property(lambda self: int(self.y1))
    
    x = lambda self, t: self.x0 + (t * self.dx)
    y = lambda self, t: self.y0 + (t * self.dy)
    
    # Position given in form (y, x) because opencv is weird 
    # for r from 0 to dr
    def pos(self, t):
        x, y = ((self.x0, self.y0) if (t < 0.5) else (self.x1, self.y1))
        
        t %= 0.5
        t *= 2
        x_cutoff = self.dx / (self.dx + self.dy)
        if t < x_cutoff:
            t /= x_cutoff
            x = self.x(t)
        else:
            t = (t - x_cutoff) / (1 - x_cutoff)
            y = self.y(t)
        
        return y, x
                   
               
# -----------------------------------------------------------------------------

