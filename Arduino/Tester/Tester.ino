const uint8_t led_1_in = 51;
const uint8_t led_2_in = 53;
const uint8_t led_1_out = 11;
const uint8_t led_2_out = 12;
const uint8_t data = 2;


void setup() {
    pinMode(led_1_in, INPUT);
    pinMode(led_2_in, INPUT);
    pinMode(led_1_out, OUTPUT);
    pinMode(led_2_out, OUTPUT);
    pinMode(data, INPUT);
    attachInterrupt(digitalPinToInterrupt(data), 
                    swapIlluminationLed, FALLING);                 
}


void loop() {}


void swapIlluminationLed() {
    digitalWrite(led_1_out, digitalRead(led_1_in));
    digitalWrite(led_2_out, digitalRead(led_2_in));
}
